using System;
using System.Threading;
using System.Timers;
using Server.network.data;
using Server.network.util;

namespace Server.network
{
    internal class GameServer
    {
        private const short ServerPort = 8088;

        public static void Main(string[] args)
        {
            Console.WriteLine("Main: start client/server");
            Console.WriteLine();

            new Thread(StartListener).Start();
            new Thread(StartConnector).Start();

            // ��������� �����
            new AutoResetEvent(false).WaitOne();
        }

        public static void StartListener()
        {
            var network = new Network(ServerPort);
            var counter = 0;
            network.OnInviteRecived += delegate(UserInfo user)
            {
                Console.WriteLine("Main: acceptor: recive invite form user: " + user.Name);
                if (counter >= 1)
                {
                    Console.WriteLine("Main: acceptor: accept invite from user: " + user.Name);
                    network.AcceptInvite();
                }
                else
                {
                    Console.WriteLine("Main: acceptor: reject invite from user: " + user.Name);
                    network.RejectInvite();
                }
                counter++;
            };

            network.OnAcceptInviteComplete += (broadcaster) =>
            {
                var simulator = new GameSimulator("acceptor", broadcaster);
                simulator.Simulate();
            };
        }

        public static void StartConnector()
        {
            Thread.Sleep(1000);
            var network = new Network(ServerPort + 1);
            string ip = "129.0.0.1";
            network.OnConnectFail += delegate
            {
                Console.WriteLine("Main: connector: fail to connect");
                ip = "127.0.0.1";
                network.ConnectTo(ip, ServerPort);
            };

            network.OnInviteAccepted += delegate(GameBroadcaster broadcaster, UserInfo user)
            {
                Console.WriteLine("Main: connector: " + user.Name + " accepted invite");
                var simulator = new GameSimulator("connector", broadcaster);
                simulator.Simulate();
            };

            network.OnInviteReject += delegate(UserInfo user)
            {
                Console.WriteLine("Main: connector: " + user.Name + "reject invite for: ");
                network.ConnectTo(ip, ServerPort);
            };
            network.ConnectTo(ip, ServerPort);
        }
    }

    internal class GameSimulator
    {
        private readonly string _target;
        private readonly GameBroadcaster _broadcaster;
        private static short _seed = 1;

        private readonly Random _random = new Random(_seed++);
        private readonly Int64 _startTime = TimeUtils.TimeStamp;

        public GameSimulator(string target, GameBroadcaster broadcaster)
        {
            _target = target;
            _broadcaster = broadcaster;
        }

        public void Simulate()
        {
            // �������� ��������� � ������
            var aTimer = new System.Timers.Timer(1000);
            aTimer.Elapsed += OnTimedEvent;
            aTimer.Enabled = true;
            GC.KeepAlive(aTimer);

            // �������� ������� ��������� � ������
            _broadcaster.OnDataRecived += info => Console.WriteLine("Main: " + _target + " recived click info:" + info);
            _broadcaster.ReadSteam();
        }

        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            var clickInfo = new ClickInfo {ClickId = _random.Next(0, 1024), Time = TimeUtils.TimeStamp - _startTime};
            Console.WriteLine("Main: send click: " + _target + ": " + clickInfo);
            _broadcaster.SendClick(clickInfo);
        }
    }
}