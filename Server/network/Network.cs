using System;
using System.Net.Sockets;
using Server.network.data;
using Server.network.socket;
using Server.network.util;

namespace Server.network
{
    // TODO: ������� ����� ��������� ������!!!
    public class Network
    {
        public Action<UserInfo> OnInviteRecived = delegate { };
        public Action<UserInfo> OnInviteRegect = delegate { };
        public Action<GameBroadcaster, UserInfo> OnInviteAccepted = delegate { };
        public Action<GameBroadcaster> OnAcceptInviteComplete = delegate { };
        public Action OnConnectFail;
        public Action OnIoError;

        private readonly UberSocket _socket = new UberSocket();
        private readonly GameBroadcaster _broadcaster = new GameBroadcaster();
        private readonly PortRange _portRange = new PortRange(1024, 8079);

        public Network(Int16 serverPort)
        {
            _broadcaster.Socket = _socket;
            _socket.OnError = OnSocketError;

            StartListen(serverPort);
        }

        private void OnSocketError(SocketException e)
        {
            switch (e.SocketErrorCode)
            {
                case SocketError.AccessDenied:
                    Console.WriteLine("network: port already in use");
                    StartListen(_portRange.Next());
                    break;
                case SocketError.TimedOut:
                    Console.WriteLine("network: fail to connect (Timed out)");
                    OnConnectFail();
                    break;
            }
        }

        private void StartListen(Int32 serverPort)
        {
            Console.WriteLine("network: try to start listerning port: " + serverPort);

            // ����� � ��� ��������������, ��������� ���������� � ���������������� ������������
            _socket.OnAccept = () => _socket.Recive<UserInfo>(user => OnInviteRecived(user));
            _socket.Listen(serverPort);
        }

        public void ConnectTo(string host, Int16 port)
        {
            Console.WriteLine("Network: connect to: " + host + ": " + port);

            _socket.OnConnect = () => _socket.Send(AppFacade.I.User, () => _socket.Recive<UserInfo>(OnInviteReason));
            _socket.Connect(host, port);
        }

        private void OnInviteReason(UserInfo user)
        {
            if (user.AcceptConfirmed)
                OnInviteAccepted(_broadcaster, user);
            else
            {
                _socket.CloseConnection();
                OnInviteRegect(user);
            }
        }

        public void AcceptInvite()
        {
            var user = AppFacade.I.User2;
            user.AcceptConfirmed = true;
            _socket.Send(user, () => OnAcceptInviteComplete(_broadcaster));
        }

        public void RejectInvite()
        {
            var userInfo = AppFacade.I.User2;
            userInfo.AcceptConfirmed = false;
            _socket.Send(userInfo, () =>
            {
                _socket.CloseConnection();
                _socket.StartAccept();
            });
        }
    }

    public class AppFacade
    {
        private static volatile AppFacade _instance;

        private static readonly Object SyncRoot = new Object();

        public AppFacade()
        {
            if (_instance != null)
                throw new InvalidOperationException("AppFacade is singleton. Use AppFacade.I property");
        }

        public static AppFacade I
        {
            get
            {
                // ������ �� ������
                if (_instance == null)
                {
                    // ���, �� ������
                    // ������ ���� ����� ����� ������� ���
                    lock (SyncRoot)
                    {
                        // ���������, �� ������ �� ������ ������ �����
                        if (_instance == null)
                        {
                            // ��� �� ������ � ������
                            _instance = new AppFacade();
                        }
                    }
                }
                return _instance;
            }
        }

        public UserInfo User
        {
            get { return new UserInfo {Name = "First User", Date = TimeUtils.TimeStamp}; }
        }

        public UserInfo User2
        {
            get { return new UserInfo {Name = "Second User", Date = TimeUtils.TimeStamp}; }
        }
    }

    internal class PortRange
    {
        private Int32 _end, _current;

        public PortRange(Int32 start, Int32 end)
        {
            _current = start;
            _end = end;
        }

        public Int32 Next()
        {
            if (++_current > _end)
                throw new IndexOutOfRangeException("��������� ����������� �����");
            return _current;
        }

        public Int32 Current
        {
            get { return _current; }
        }
    }
}