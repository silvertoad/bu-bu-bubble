﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Server.network.data
{
    [Serializable]
    public class ClickInfo
    {
        public Int32 ClickId;
        public Int64 Time;

        public byte[] ToBytes()
        {
            var memorySteam = new MemoryStream();
            var formatter = new BinaryFormatter();
            formatter.Serialize(memorySteam, this);
            return memorySteam.GetBuffer();
        }

        public override string ToString()
        {
            return string.Format("[ClickInfo => ClickId: " + ClickId + ", Time: " + Time + "]");
        }
    }
}
