using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Server.network.data
{
	[Serializable]
	public class UserInfo
	{
		public string Name;
        public Int64 Date;
	    public Boolean AcceptConfirmed = true;

	    public byte[] ToBytes()
	    {
            var memorySteam = new MemoryStream();
            var formatter = new BinaryFormatter();
            formatter.Serialize(memorySteam, this);
            return memorySteam.GetBuffer();
	    }

		public override string ToString ()
		{
			return string.Format ("[UserInfo => Name: " + Name + ", Date: " + Date + ", AcceptConfirmed: " + AcceptConfirmed + "]");
		}
	}
}