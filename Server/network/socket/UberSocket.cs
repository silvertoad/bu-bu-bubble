﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;

namespace Server.network.socket
{
    // TODO: сделать обработку ошибок
    public class UberSocket
    {
        public Action OnAccept;
        public Action OnConnect;

        public Action<SocketException> OnError;

        private readonly MessageReciver _reciver = new MessageReciver();
        private readonly MessageSender _sender = new MessageSender();
        private readonly Socket _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        public void Listen(Int32 port)
        {
            var endPoint = new IPEndPoint(IPAddress.Any, port);

            try
            {
                _socket.Bind(endPoint);
                _socket.Listen(10);
                StartAccept();
            }
            catch (SocketException e)
            {
                OnError(e);
            }
        }

        public void StartAccept()
        {
            var socketAsyncEventArgs = new SocketAsyncEventArgs();
            socketAsyncEventArgs.Completed += (sender, args) => InitSockets(args.AcceptSocket, OnAccept);
            _socket.AcceptAsync(socketAsyncEventArgs);
        }

        private void InitSockets(Socket socket, Action callback)
        {
            // TODO: проверить все ли отписалось
            _sender.SetSocket(socket);
            _reciver.SetSocket(socket);
            callback();
        }

        public void Connect(string host, Int32 port)
        {
            var endPoint = new IPEndPoint(IPAddress.Parse(host), port);
            var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            var socketAsyncEventArgs = new SocketAsyncEventArgs {RemoteEndPoint = endPoint};

            socketAsyncEventArgs.Completed += (sender, args) =>
            {
                if (args.ConnectSocket == null && args.SocketError != SocketError.Success)

                    OnError(new SocketException(10060));
                else
                    InitSockets(args.ConnectSocket, OnConnect);
            };

            socket.ConnectAsync(socketAsyncEventArgs);
        }

        public void Send(object message, Action onComplete)
        {
            _sender.OnComplete = delegate
            {
                Console.WriteLine("SEND: " + message);
                onComplete();
            };
            try
            {
                _sender.Send(message);
            }
            catch (SocketException e)
            {
                OnError(e);
            }
        }

        public void Recive<TReciveType>(Action<TReciveType> onComplete)
        {
            _reciver.OnComplete = result =>
            {
                Console.WriteLine("RECIVE: " + result);
                onComplete((TReciveType) result);
            };
            try
            {
                _reciver.Recive();
            }
            catch (SocketException e)
            {
                OnError(e);
            }
        }

        public void CloseConnection()
        {
            _sender.Close();
            _reciver.Close();
        }
    }

    internal class MessageSender : BaseMessageIteraction
    {
        private readonly BinaryFormatter _formatter = new BinaryFormatter();
        public Action OnComplete;

        private byte[] _byteMessage;

        public void Send(object message)
        {
            var memStream = new MemoryStream();
            _formatter.Serialize(memStream, message);
            _byteMessage = memStream.GetBuffer();

            byte[] messageLength = BitConverter.GetBytes(_byteMessage.Length);
            var args = new MessageSteamArgs(messageLength, CompleteSendHeader);
            Socket.SendAsync(args);
        }

        private void CompleteSendHeader()
        {
            var args = new MessageSteamArgs(_byteMessage, OnComplete);
            Socket.SendAsync(args);
        }
    }

    internal class MessageReciver : BaseMessageIteraction
    {
        public const short HeaderLength = 4;

        private readonly BinaryFormatter _formatter = new BinaryFormatter();
        public Action<object> OnComplete;
        private byte[] _recived;

        public void Recive()
        {
            _recived = new byte[HeaderLength];
            var args = new MessageSteamArgs(_recived, CompleteReciveHeader);
            Socket.ReceiveAsync(args);
        }

        private void CompleteReciveHeader()
        {
            Int32 messageLength = BitConverter.ToInt32(_recived, 0);
            _recived = new byte[messageLength];
            var args = new MessageSteamArgs(_recived, CompleteDataRecive);
            Socket.ReceiveAsync(args);
        }

        private void CompleteDataRecive()
        {
            var memStream = new MemoryStream(_recived);
            object data = _formatter.Deserialize(memStream);
            OnComplete(data);
        }
    }

    internal class BaseMessageIteraction
    {
        protected Socket Socket;

        public void SetSocket(Socket socket)
        {
            // TODO: отписаться от всего связанного с предыдущим сокетом
            if (Socket != null)
                Socket.Close();
            Socket = socket;
        }

        public void Close()
        {
            Socket.Close();
        }
    }

    internal class MessageSteamArgs : SocketAsyncEventArgs
    {
        public MessageSteamArgs(byte[] buffer, Action onComplete)
        {
            SetBuffer(buffer, 0, buffer.Length);
            Completed += (sender, eventArgs) => onComplete();
        }
    }
}