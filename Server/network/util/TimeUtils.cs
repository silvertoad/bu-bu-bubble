﻿using System;

namespace Server.network.util
{
    class TimeUtils
    {
        public static Int64 TimeStamp
        {
            get
            {
                return (Int64)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalMilliseconds;
            }
        }
    }
}
