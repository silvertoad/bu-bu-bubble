﻿using System;
using System.Collections.Generic;
using System.Linq;
using Server.network.data;
using Server.network.socket;

namespace Server.network
{
    public class GameBroadcaster
    {
        public Action<ClickInfo> OnDataRecived = delegate { };

        public UberSocket Socket { get; set; }

        private readonly List<ClickInfo> _queue = new List<ClickInfo>();
        private Boolean _writeStreamFree = true;
        private Boolean _readSreamFree = true;

        public void SendClick(ClickInfo clickInfo)
        {
            _queue.Add(clickInfo);
            SendClick();
        }

        private void SendClick()
        {
            var canWrite = _queue.Count > 0 && _writeStreamFree;
            if (!canWrite) return;

            _writeStreamFree = false;
            var toSend = _queue.First();
            _queue.RemoveAt(0);
            Socket.Send(toSend, () => { _writeStreamFree = true; SendClick(); });
        }

        public void ReadSteam()
        {
            if (!_readSreamFree) return;

            _readSreamFree = false;
            Socket.Recive<ClickInfo>(info =>
            {
                _readSreamFree = true; 
                OnDataRecived(info);
                ReadSteam();
            });
        }
    }
}