=== Happy Tree Friends Icon Set ===

By: Sirea - Martina �mejkalov� (http://www.sireasgallery.com/) 
Contact: m.smejkalova@gmail.com

Download: http://www.sireasgallery.com/iconset/happytreefriends/

Description: 3D version of the cute monsters from Happy Tree Friends series. 
I like the characters, but the series is too brutal for me.

==========

Creative Commons - Attribution

You are free:

* To Share - To copy, distribute and transmit the work.
* To Remix - To adapt the work.

Under the following conditions:

* Attribution - You must attribute the work in the manner specified
  by the author or licensor (but not in any way that suggests that
  they endorse you or your use of the work). For example, if you are
  making the work available on the Internet, you must link to the
  original source.