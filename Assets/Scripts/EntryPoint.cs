﻿using System;
using Assets.Scripts;
using Assets.Scripts.Models;
using BubblesGame.GUI;
using BubblesGame.Utils;
using BubblesGame.View;
using UnityEngine;

namespace BubblesGame
{
    /// <summary>
    /// Точка входа в приложение
    /// </summary>
    internal class EntryPoint : MonoBehaviour
    {
        public static event Action OnUpdateOnce;
        public static event Action OnUpdate = delegate { };

        /// <summary>
        /// Вспомогательная функция для выполнения некой логики в майн-треде Unity
        /// </summary>
        /// <param name="d"></param>
        public static void MakeOnNextUpdate(Action d)
        {
            OnUpdateOnce += d;
        }

        private void Start()
        {
            Application.runInBackground = true;
            Debug.Log("START");

            var soundManager = new SoundManager();
            soundManager.SoundDataLoaded += OnSoundDataLoaded;
            StartCoroutine(soundManager.PrepareBundle());

            var bundleWrapper = new BundleWrapper();
            bundleWrapper.GameDataLoaded += OnGameDataLoaded;
            StartCoroutine(bundleWrapper.ProcessGamePack());

            gameObject.AddComponent<CameraManager>();

            // Init Facade
            Facade.I.Init("Player 1", 1);
        }

        private void OnApplicationQuit()
        {
            Debug.Log("EXIT");
            Facade.I.Net.Close();
        }

        private void Update()
        {
            if (OnUpdateOnce != null)
            {
                OnUpdateOnce();
                OnUpdateOnce = null;
            }
            OnUpdate();
        }

        private void OnGameDataLoaded()
        {
            Debug.Log("GAME DATA LOADED");
            new ScreenManager().Init(ScreenType.Login, null);
        }

        private void OnSoundDataLoaded()
        {
            Debug.Log("SOUND DATA LOADED");
            SoundManager.PlaySound(gameObject.AddComponent<AudioSource>(), SoundManager.MAIN_THEME, true);
        }
    }
}
