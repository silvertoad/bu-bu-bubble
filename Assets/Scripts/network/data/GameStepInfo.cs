﻿using System;

namespace Assets.Scripts.network.data
{
    [Serializable]
    public class GameStepInfo
    {
        public int ClickId;
        public float Time;
        
        public override string ToString()
        {
            return string.Format("[GameStepInfo => ClickId: " + ClickId + ", Time: " + Time + "]");
        }
    }
}
