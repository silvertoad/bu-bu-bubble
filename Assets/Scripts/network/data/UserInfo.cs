using System;

namespace Assets.Scripts.network.data
{
    [Serializable]
    public class UserInfo : IUserModel
    {
        private string UserName;
        private Int32 UserSeed;
        public Boolean AcceptConfirmed { get; private set; }

        public UserInfo(string name, Int32 seed)
        {
            UserName = name;
            UserSeed = seed;
        }
        public UserInfo(IUserModel clone, Boolean accepted) : this(clone.Name, clone.Seed)
        {
            AcceptConfirmed = accepted;
        }

        public override string ToString()
        {
            return string.Format("[UserInfo => "
                                 + "Name: " + Name
                                 + ", AcceptConfirmed: " + AcceptConfirmed
                                 + ", UserSeed: " + UserSeed
                                 + "]");
        }

        public string Name
        {
            get { return UserName; }
            set { UserName = value; }
        }

        public int Seed
        {
            get { return UserSeed; }
        }

        public UserInfo ToSerializable(bool accepted = true)
        {
            return new UserInfo(this, accepted);
        }
    }
}