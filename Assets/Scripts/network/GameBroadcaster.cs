﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.network.data;
using Assets.Scripts.network.socket;
using Debug = UnityEngine.Debug;

namespace Assets.Scripts.network
{
    /// <summary>
    /// Обертка отправляющая и принимающая сообщения от/к удаленного приложения
    /// </summary>
    public class GameBroadcaster
    {
        /// <summary>
        /// Ивент принятия данных от удаленного клиента
        /// </summary>
        public Action<GameStepInfo> OnDataRecived = delegate { };
        
        /// <summary>
        /// Завершение очереди отправления сообщений (необходимо на этами конца сетевой игры)
        /// </summary>
        public Action QueueComplete = delegate { };

        private readonly UberSocket _socket;
        private List<GameStepInfo> _queue;
        private Boolean _writeStreamFree = true;
        private Boolean _readSreamFree = true;

        /// <summary>
        /// Конструктор, запоминает ссылку на сокет
        /// </summary>
        /// <param name="socket">Сокет через который происзодит отправка/принятие данных</param>
        public GameBroadcaster(UberSocket socket)
        {
            _socket = socket;
        }

        /// <summary>
        /// Метод подготавливает класс к новой игре
        /// </summary>
        /// <returns>Текучий интерфейс</returns>
        public GameBroadcaster Reinit()
        {
            _queue = new List<GameStepInfo>();
            _writeStreamFree = true;
            _readSreamFree = true;
            return this;
        }

        /// <summary>
        /// Добавить в очередь отправки удаленному приложению игровой шаг
        /// </summary>
        /// <param name="gameStepInfo">Подготовленный к сериализации игровой шаг</param>
        public void SendGameStep(GameStepInfo gameStepInfo)
        {
            _queue.Add(gameStepInfo);
            SendGameStep();
        }

        /// <summary>
        /// Проверить и отправить игровой шаг удаленному приложению
        /// </summary>
        private void SendGameStep()
        {
            var canWrite = _queue.Count > 0 && _writeStreamFree;
            if (!canWrite) return;

            _writeStreamFree = false;
            var toSend = _queue.First();
            _queue.RemoveAt(0);
            _socket.Send(toSend, () =>
            {
                _writeStreamFree = true;
                SendGameStep();
                if (_queue.Count == 0)
                {
                    QueueComplete();
                }
            });
        }

        /// <summary>
        /// Прочитать игровой шаг пришедщий от удленного приложения
        /// </summary>
        public void ReadSteam()
        {
            if (!_readSreamFree) return;

            _readSreamFree = false;
            _socket.Recive<GameStepInfo>(info =>
            {
                _readSreamFree = true;
                OnDataRecived(info);
                ReadSteam();
            });
        }

        /// <summary>
        /// Остановить отправку/принятие сообщений
        /// </summary>
        public void Stop()
        {
            _writeStreamFree = false;
            _readSreamFree = false;
            Debug.Log("stop broadcaster");
        }

        /// <summary>
        /// Количество сообщений ожидающих отправки
        /// </summary>
        public int QueueCount
        {
            get { return _queue.Count; }
        }
    }
}