using System;
using System.Collections.Generic;
using Assets.Scripts.network.data;
using Assets.Scripts.network.socket;
using BubblesGame;
using BubblesGame.Models.Interfaces;
using Debug = UnityEngine.Debug;

namespace Assets.Scripts.network
{
    public class Inbox
    {
        public event Action Update = delegate { };

        private List<InboxMessage> messages = new List<InboxMessage>();

        public void Add(InboxMessage message)
        {
            EntryPoint.OnUpdate += UpdateMessages;
            message.OnConfirm += Remove;
            messages.Add(message);
            Update();
        }

        public void Remove(InboxMessage message)
        {
            message.OnConfirm -= Remove;
            messages.Remove(message);
            Update();
            if(messages.Count == 0 )
                EntryPoint.OnUpdate -= UpdateMessages;
        }

        public IList<InboxMessage> FindMessagesBySender(IUserModel sender)
        {
            List<InboxMessage> list = new List<InboxMessage>();
            messages.ForEach((item) => { if (item.Sender == sender) list.Add(item); });
            return list;
        }

        public void RemoveAll(IUserModel sender, Type messageType) 
        {
            IList<InboxMessage> list = FindMessagesBySender(sender);
            for (int i = 0; i < list.Count; i++)
            {
                InboxMessage message = list[i];
                if (message.GetType() == messageType)
                {
                    message.OnConfirm -= Remove;
                    messages.Remove(message);
                }
            }
            Update();
        }

        private void UpdateMessages()
        {
            for (int i = 0; i < messages.Count; i++)
            {
                InboxMessage message = messages[i];
                if (message is InboxConnection)
                {
                    var connection = (InboxConnection) message;
                    connection.CheckConnection();
                }
            }
        }

        public int Count { get { return messages.Count; } }

        public IList<InboxMessage> GetMessages()
        {
            var list = new List<InboxMessage>();
            messages.ForEach((item) => list.Add(item));
            return list;
        }
    }

    public class InboxMessage
    {
        public event Action<InboxMessage> OnOpen = delegate { };
        public event Action<InboxMessage> OnConfirm = delegate { };

        public InboxMessage(IUserModel sender)
        {
            Sender = sender;
        }

        public IUserModel Sender { get; private set; }
        public bool IsReaded { get; private set; }
        public bool IsSkipped { get; private set; }
        public bool IsAccepted { get; private set; }
        public bool IsConfirmed { get { return IsSkipped || IsAccepted; } }

        public void Skip()
        {
            if (IsReaded && !IsConfirmed)
            {
                IsSkipped = true;
                OnConfirm(this);
            }
        }

        public void Accept()
        {
            if (IsReaded && !IsConfirmed)
            {
                IsAccepted = true;
                OnConfirm(this);
            }
        }

        public InboxMessage Open()
        {
            IsReaded = true;
            OnOpen(this);
            return this;
        }
    }

    public class InboxConnection : InboxMessage
    {
        private UberSocket _socket;

        public InboxConnection(IUserModel sender, UberSocket socket)
            : base(sender)
        {
            _socket = socket;
        }
        
        public void CheckConnection()
        {
            Debug.Log("OnClose message");
            if (!_socket.Reciver.IsConnected)
            {
                _socket = null;
                Facade.I.Net.Disconnect();
                Open().Skip();
            }
        }
    }
}
