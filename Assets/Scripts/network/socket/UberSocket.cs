using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace Assets.Scripts.network.socket
{
    ///<summary>Класс обрабатывающий TCP соединения с другими клиентами.
    ///Слушает, Соединяется, Читает/Пишет в соответствующие сокеты.</summary>
    public class UberSocket
    {
        ///<summary>Событие принятия коннекта от удаленного клиента</summary>
        public Action OnAccept;

        ///<summary>Событие спешного коннекта к удаленному клиенту</summary>
        public Action OnConnect;

        ///<summary>Событие ошибки во время сетевого взаимодействия</summary>
        public Action<SocketException> OnError;

        ///<summary>Событие закрытия конекта</summary>
        public Action OnCloseConnection = delegate { };

        public const string Localhost = "127.0.0.1";

        private readonly MessageReciver _reciver = new MessageReciver();
        private readonly MessageSender _sender = new MessageSender();
        private readonly Socket _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        private Int32 _listeningPort;

        private void InitSockets(Socket socket, Action callback)
        {
            EventArgsFactory.OnError += () => OnCloseConnection();
            _sender.SetSocket(socket);
            _reciver.SetSocket(socket);
            callback();
        }

        ///<summary>Попытаться начать прослушивание порта <paramref name="port"/>.</summary>
        ///<param name="port">Прослушиваемый порт</param>
        public void Listen(Int32 port)
        {
            var endPoint = new IPEndPoint(IPAddress.Any, port);

            try
            {
                _socket.Bind(endPoint);
                _socket.Listen(10);
                StartAccept();
                _listeningPort = port;
            }
            catch (SocketException e)
            {
                Debug.Log(e);
                OnError(e);
            }
        }

        ///<summary>Закрыть все соединения и остановить прослушивание.</summary>
        /// <remarks>Нужно вызывать при закрытии приложения.</remarks>
        public void Close()
        {
            _socket.Close();
            CloseConnection();
        }

        ///<summary>Стартует принятие конектов</summary>
        public void StartAccept()
        {
            _socket.BeginAccept(ar =>
            {
                var socket = (Socket) ar.AsyncState;
                var accepted = socket.EndAccept(ar);
                InitSockets(accepted, OnAccept);
            }, _socket);
        }

        /// <summary>
        /// Попробовать совершить коннект к удаленному клиенту
        /// </summary>
        /// <param name="host">IP удаленного клиента</param>
        /// <param name="port">номер порта удаленного клиента</param>
        public void Connect(string host, Int32 port)
        {
            var endPoint = new IPEndPoint(IPAddress.Parse(host), port);
            var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            var socketAsyncEventArgs = new SocketAsyncEventArgs {RemoteEndPoint = endPoint};

            socketAsyncEventArgs.Completed += (sender, args) =>
            {
                Boolean isLocalListenerHost = host == Localhost && _listeningPort == port;
                if (args.SocketError != SocketError.Success || isLocalListenerHost)
                    OnError(new SocketException(10061));
                else
                    InitSockets((Socket) sender, OnConnect);
            };

            socket.ConnectAsync(socketAsyncEventArgs);
        }

        /// <summary>
        /// Асинхронно отправить сообщение
        /// </summary>
        /// <param name="message">Отправляемое сообщение</param>
        /// <param name="onComplete">Колбэк завершения отправки сообщения</param>
        public void Send(object message, Action onComplete)
        {
            _sender.OnComplete = onComplete;

            try
            {
                _sender.Send(message);
            }
            catch (SocketException e)
            {
                OnError(e);
            }
        }

        /// <summary>
        /// Асинхронно принять сообщение
        /// </summary>
        /// <typeparam name="TReciveType">Тип принимаемого сообщения</typeparam>
        /// <param name="onComplete">Колбэк завершения принятия сообщения</param>
        public void Recive<TReciveType>(Action<TReciveType> onComplete)
        {
            _reciver.OnComplete = result => onComplete((TReciveType) result);

            try
            {
                _reciver.Recive();
            }
            catch (SocketException e)
            {
                OnError(e);
            }
        }

        /// <summary>
        /// Закрыть установленое соединение с удаленным клиентом
        /// </summary>
        public void CloseConnection()
        {
            Debug.Log("Close connection");
            _sender.Close();
            _reciver.Close();
        }

        public MessageReciver Reciver
        {
            get { return _reciver; }
        }
    }

    /// <summary>
    /// Асинхронная обертка определяющая интерфейс для отправки сообщений удаленноve клиентe
    /// </summary>
    public class MessageSender : BaseMessageIteraction
    {
        /// <summary>
        /// Событие завершения отправки сообщения
        /// </summary>
        public Action OnComplete;

        private readonly BinaryFormatter _formatter = new BinaryFormatter();

        private byte[] _byteMessage;

        /// <summary>
        /// Начать отправку сообщения
        /// </summary>
        /// <param name="message">Отправляемое сообщение. Должно быть промаркировано атрибутом [Serializable]</param>
        public void Send(object message)
        {
            var memStream = new MemoryStream();
            _formatter.Serialize(memStream, message);
            _byteMessage = memStream.GetBuffer();

            var messageLength = BitConverter.GetBytes(_byteMessage.Length);
            var args = EventArgsFactory.Create(messageLength, CompleteSendHeader);
            Socket.SendAsync(args);
        }

        private void CompleteSendHeader()
        {
            var args = EventArgsFactory.Create(_byteMessage, OnComplete);
            Socket.SendAsync(args);
        }
    }

    /// <summary>
    /// Асинхронная обертка определяющая интерфейс для приема сообщений от удаленного клиента
    /// </summary>
    public class MessageReciver : BaseMessageIteraction
    {
        /// <summary>
        /// Размер заголовка сообщения
        /// </summary>
        public const short HeaderLength = 4;

        /// <summary>
        /// Событие завершения приема сообщения
        /// </summary>
        public Action<object> OnComplete;

        private readonly BinaryFormatter _formatter = new BinaryFormatter();
        private byte[] _recived;



        /// <summary>
        /// Начать прием сообщения
        /// </summary>
        public void Recive()
        {
            _recived = new byte[HeaderLength];
            var args = EventArgsFactory.Create(_recived, CompleteReciveHeader);
            Socket.ReceiveAsync(args);
        }

        private void CompleteReciveHeader()
        {
            Int32 messageLength = BitConverter.ToInt32(_recived, 0);
            _recived = new byte[messageLength];
            var args = EventArgsFactory.Create(_recived, CompleteDataRecive);
            Socket.ReceiveAsync(args);
        }

        private void CompleteDataRecive()
        {
            var memStream = new MemoryStream(_recived);
            object data = _formatter.Deserialize(memStream);
            OnComplete(data);
        }
    }

    /// <summary>
    ///  Базовый класс для оберток отправки/принятия сообщений
    /// </summary>
    public class BaseMessageIteraction
    {
        protected Socket Socket;

        /// <summary>
        /// Закрывает предыдущий установленный сокет, устанавливает новый
        /// </summary>
        /// <param name="socket">Сокет через который будет происходить операции отправки/принятия сообщений</param>
        public void SetSocket(Socket socket)
        {
            if (Socket != null)
                Socket.Close();
            Socket = socket;
        }

        /// <summary>
        /// Закрыть соединение
        /// </summary>
        public void Close()
        {
            if (Socket != null)
            {
                Socket.Close();
                Socket = null;
            }
        }

        /// <summary>
        /// Возвращает статус коннекта связанного с оберткой сокета
        /// </summary>
        public Boolean IsConnected
        {
            get
            {
                try
                {
                    return !(Socket.Poll(1, SelectMode.SelectRead) && Socket.Available == 0);
                }
                catch (SocketException)
                {
                    return false;
                }
            }
        }
    }

    /// <summary>
    /// Обертка над SocketAsyncEventArgs, созданная для сокращения кода
    /// </summary>
    internal class EventArgsFactory
    {
        public static event Action OnError;

        public static SocketAsyncEventArgs Create(byte[] buffer, Action onComplete)
        {
            var args = new SocketAsyncEventArgs();
            args.SetBuffer(buffer, 0, buffer.Length);
            args.Completed += (sender, eventArgs) =>
            {
                if (eventArgs.SocketError != SocketError.Success)
                {
                    Debug.Log(eventArgs.SocketError);
                    OnError();
                }
                else
                    onComplete();
            };

            return args;
        }

    }
}