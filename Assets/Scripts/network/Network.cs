using System;
using System.Net;
using System.Net.Sockets;
using Assets.Scripts.network.data;
using Assets.Scripts.network.socket;
using Debug = UnityEngine.Debug;

namespace Assets.Scripts.network
{
    /// <summary>
    /// Класс обрабабатывающий сетевые взаимодействия
    /// </summary>
    public class Network
    {
        public Action<UserInfo> OnInviteRecived;
        public Action<UserInfo> OnInviteReject;
        public Action<GameBroadcaster, IUserModel> OnInviteAccepted;
        public Action<GameBroadcaster> OnAcceptInviteComplete;
        public Action OnConnectFail;
        public Action OnIoError;
        public Action OnConnectionRefuse = delegate {  };

        private readonly UberSocket _socket = new UberSocket();
        private readonly GameBroadcaster _broadcaster;
        private readonly PortRange _portRange;

        /// <summary>
        /// Конструктор. Иинициализирует прослушивание порта. Изначально пытается прослущать <paramref name="serverPort"/>
        /// </summary>
        /// <param name="serverPort">Прослушиваемый порт приложения</param>
        public Network(Int16 serverPort)
        {
            _socket.OnCloseConnection += delegate
            {
                Debug.Log("network close connection");
                OnConnectionRefuse();
            };
            _portRange = new PortRange(1024, 8079, serverPort);
            _broadcaster = new GameBroadcaster(_socket);
            _socket.OnError = OnSocketError;

            StartListen(serverPort);
        }

        /// <summary>
        /// Соединиться с удаленным приложением
        /// </summary>
        /// <param name="host">ip удаленного приложения</param>
        /// <param name="port">порт удаленного приложения</param>
        public void ConnectTo(string host, Int16 port)
        {
            Debug.Log("Network: connect to: " + host + ": " + port);

            var userInfo = Facade.I.User.ToSerializable();
            _socket.OnConnect = () => _socket.Send(userInfo, () => _socket.Recive<UserInfo>(OnInviteReason));
            _socket.Connect(host, port);
        }

        /// <summary>
        /// Отправить решение о инвайте пользователю
        /// </summary>
        /// <param name="accepted">Принять или отклонить инвайт</param>
        public void ResolveInvite(Boolean accepted)
        {
            var user = Facade.I.User.ToSerializable(accepted);
            if (accepted)
                _socket.Send(user, () => OnAcceptInviteComplete(_broadcaster.Reinit()));
            else
                _socket.Send(user, delegate
                {
                    _socket.CloseConnection();
                    _socket.StartAccept();
                });
        }

        /// <summary>
        /// Текущий прослушиваемый порт
        /// </summary>
        public Int32 ListeningPort
        {
            get { return _portRange.Current; }
        }

        /// <summary>
        /// IP адрес пользователя
        /// </summary>
        public String Ip
        {
            get
            {
                var localIp = "";
                var host = Dns.GetHostEntry(Dns.GetHostName());
                foreach (var ip in host.AddressList)
                {
                    if (ip.AddressFamily == AddressFamily.InterNetwork)
                    {
                        localIp = ip.ToString();
                        break;
                    }
                }
                return localIp;
            }
        }

        /// <summary>
        /// IP + порт пользователя
        /// </summary>
        public String Addres
        {
            get { return Ip + ":" + ListeningPort; }
        }

        /// <summary>
        /// Отключить подключенное
        /// </summary>
        public void Disconnect()
        {
            _broadcaster.Stop();
            _socket.CloseConnection();
            _socket.StartAccept();
        }

        /// <summary>
        /// Закрыть все соединения
        /// </summary>
        public void Close()
        {
            _socket.Close();
        }

        private void OnInviteReason(UserInfo user)
        {
            if (user.AcceptConfirmed)
                OnInviteAccepted(_broadcaster.Reinit(), user);
            else
            {
                _socket.CloseConnection();
                OnInviteReject(user);
            }
        }

        private void OnSocketError(SocketException e)
        {
            switch (e.SocketErrorCode)
            {
                case SocketError.AddressAlreadyInUse:
                    Debug.Log("network: port already in use");
                    StartListen(_portRange.Next());
                    break;
                case SocketError.ConnectionRefused:
                    Debug.Log("network: fail to connect");
                    OnConnectFail();
                    break;
            }
        }

        private void StartListen(Int32 serverPort)
        {
            Debug.Log("network: try to start listerning port: " + serverPort);

            // Когда к нам приконектились, принимаем информацию о присоеденившемся пользователе
            _socket.OnAccept = () => _socket.Recive<UserInfo>(user =>
            {
                var inboxConnect = new InboxConnection(user, _socket);
                Facade.I.Inbox.Add(inboxConnect);
                OnInviteRecived(user);
            });
            _socket.Listen(serverPort);
        }
    }

    /// <summary>
    /// Класс содержащий границы поиска номера порта на котором будет произованиться прослушивание + текущий прослушиваемый порт
    /// </summary>
    internal class PortRange
    {
        private readonly Int32 _start;
        private readonly Int32 _end;
        private Int32 _current;
        private Boolean _isMoved;

        /// <summary>
        /// Конструктор задающие границы портов + начальное значение порт
        /// </summary>
        /// <param name="start">Нижняя граница порта</param>
        /// <param name="end">Верхняя граница порта</param>
        /// <param name="init">Начальное значение порта</param>
        public PortRange(Int32 start, Int32 end, Int32 init)
        {
            _current = init;
            _start = start;
            _end = end;
        }

        /// <summary>
        ///  Вернуть следующий номер порт, на который будет произведена попытка установить прослушивание
        /// </summary>
        /// <returns>Номер порта</returns>
        public Int32 Next()
        {
            _current = _isMoved ? _current : _start;
            _isMoved = true;

            if (++_current > _end)
                throw new IndexOutOfRangeException("Кночились разрешенные порты");
            return _current;
        }

        /// <summary>
        /// Последний установленный порт
        /// </summary>
        public Int32 Current
        {
            get { return _current; }
        }
    }
}