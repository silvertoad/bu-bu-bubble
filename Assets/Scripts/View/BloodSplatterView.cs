﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;
using BubblesGame.Utils;
using Random = UnityEngine.Random;

namespace BubblesGame.View
{
    /// <summary>
    /// Отображение "кровавого пятна"
    /// </summary>
    class BloodSplatterView : MonoBehaviour
    {
        private Tweener _colorTweener;
        private Tweener _scaleTweener;

        public void Init(Material material)
        {
            gameObject.renderer.material = material;
            gameObject.collider.enabled = false;
            gameObject.GetComponent<MeshCollider>().enabled = false;

            gameObject.transform.Rotate(-90, 0, 0);
            gameObject.transform.Rotate(Vector3.up, Random.Range(-180, 180));

            var fromColor = gameObject.renderer.material.color;
            var toColor = new Color(fromColor.r, fromColor.g, fromColor.b, 0f);

            _colorTweener = new Tweener(gameObject.renderer.material)
                .Add(new Tween("color", TweenType.Lerp, fromColor, toColor, 5000, 30000))
                .Start();

            _colorTweener.Complete += ColorComplete;

            var fromScale = new Vector3(0, 0, 0);
            var toScale = gameObject.transform.localScale;

            _scaleTweener = new Tweener(gameObject.transform)
                .Add(new Tween("localScale", TweenType.Lerp, fromScale, toScale, 140))
                .Start();

            _scaleTweener.Complete += ScaleComplete;
        }

        private void ScaleComplete(Tweener tweener)
        {
            _scaleTweener = null;
        }

        private void ColorComplete(Tweener tweener)
        {
            _colorTweener = null;
            Destroy(gameObject);
        }

        public void Update()
        {
            _colorTweener.Update();
            if (_scaleTweener != null)
                _scaleTweener.Update();
        }
    }
}
