﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BubblesGame.View
{
    /// <summary>
    /// Менеджер камеры, который следит за размерами экрана и делает так, чтобы экранные координаты совпадали с координатами Unity
    /// </summary>
    class CameraManager : MonoBehaviour
    {
        private Camera _camera;
        private int _lastWidth;
        private int _lastHeight;

        private void Awake()
        {
            _camera = (Camera)FindObjectOfType(typeof(Camera));
            _camera.transform.eulerAngles = new Vector3(0,0,180);
            ManageCamera();
        }

        private void ManageCamera()
        {
            var w = (float) Screen.width;
            var h = (float) Screen.height;

            _camera.orthographicSize = h / 2;
            _camera.transform.position = new Vector3(w / 2, h / 2, -_camera.orthographicSize);    
        }

        private void Update()
        {
            if (_lastHeight != Screen.width || _lastWidth != Screen.width)
            {
                _lastWidth = Screen.width;
                _lastHeight = Screen.height;
                ManageCamera();
            }
        }
    }
}
