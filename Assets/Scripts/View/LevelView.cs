﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Assets.Scripts.View;
using BubblesGame.Models.Interfaces;
using BubblesGame.Utils;
using UnityEngine;
using Random = UnityEngine.Random;

namespace BubblesGame.View
{
    /// <summary>
    /// Отображение игрового уровня
    /// </summary>
    public class LevelView : MonoBehaviour, IGameArea 
    {
        private IGameLevel _level;
        private ILevelController _controller;
        private IGameObjectViewFactory _viewFactory;


        private readonly Dictionary<IGameObjectModel, BallView> _objects;  
        private Material _splatterMaterial;
        private GameObject _scoreTextPrefab;

        private ResourceManager _resourceManager;
        
        /// <summary>
        /// Левая граница экрана, от 0 до 1 (относительно ширины экрана)
        /// </summary>
        public float Left { get; private set; }

        /// <summary>
        /// Правая граница экрана, от 0 до 1 (относительно ширины экрана)
        /// </summary>
        public float Right { get; private set; }

        public LevelView()
        {
            Left = 0f;
            Right = 1f;
            _objects = new Dictionary<IGameObjectModel, BallView>();
        }

        /// <summary>
        /// Установка менеджера ресурсов
        /// </summary>
        /// <param name="resourceManager"></param>
        public void SetResourceManager(ResourceManager resourceManager)
        {
            _resourceManager = resourceManager;
            _scoreTextPrefab = _resourceManager.GetGameObject("ScoreText");
        }
        
        /// <summary>
        /// Инициализатор
        /// </summary>
        /// <param name="level"></param>
        /// <param name="controller"></param>
        /// <param name="viewFactory"></param>
        public void Init(IGameLevel level, ILevelController controller, IGameObjectViewFactory viewFactory)
        {
            _viewFactory = viewFactory;
            _level = level;
            _controller = controller;
        
            _level.NewObjectCreated += OnNewObjectCreated;
            _level.ObjectRemoved += OnObjectRemoved;

            var shader = Shader.Find("Transparent/Diffuse");
            if (shader == null) throw new Exception("CANT FIND SHADER");
            
            _splatterMaterial = new Material(shader);
            _splatterMaterial.mainTexture = _resourceManager.GetTexture("blot_1");
        }

        private void OnObjectRemoved(IGameObjectModel obj)
        {
            var view = _objects[obj];
            ShowBloodSplatter(view.transform);
            ShowScores(view.transform, obj.Score);
        }
        
        /// <summary>
        /// Установить размер игрового поля
        /// </summary>
        /// <param name="left">левая граница экрана, от 0 до 1</param>
        /// <param name="right">правая граница экрана, от 0 до 1</param>
        public void SetSpace(float left, float right)
        {
            Left = left;
            Right = right;
        }

        public void Update()
        {
            if (_controller != null)
                _controller.Rewind(Time.deltaTime);
            
            _resourceManager.Update();
        }

        /// <summary>
        /// Инициализировать новый игровой объект, навешать обработчики
        /// Задается рандомный поворот
        /// </summary>
        /// <param name="gameObjectModel"></param>
        private void OnNewObjectCreated(IGameObjectModel gameObjectModel)
        {
            var obj = _viewFactory.Instantiate();
            obj.transform.parent = gameObject.transform;

            obj.AddComponent<Rigidbody>();
            obj.rigidbody.AddTorque(Random.Range(-7, 7), Random.Range(-7, 7), Random.Range(-7, 7));
            
            var ballView = obj.AddComponent<BallView>();
            ballView.MouseDown += MoveDownOnMouseDown;
            ballView.Init(gameObjectModel, this, _resourceManager.GetTexture(gameObjectModel.TextureSize, gameObjectModel.Clr)); 

            _objects.Add(gameObjectModel, ballView);
        }

        private void MoveDownOnMouseDown(BallView ballView)
        {
            _controller.OnMouseDown(ballView.id);
        }

        /// <summary>
        /// Отобразить кровавое пятно на месте лопнувшего шара
        /// </summary>
        /// <param name="transform">Трансформация шара</param>
        private void ShowBloodSplatter(Transform transform)
        {
            var plane = GameObject.CreatePrimitive(PrimitiveType.Plane);
            plane.transform.position = new Vector3(transform.position.x, transform.position.y, 5000);
            plane.transform.localScale = transform.localScale / 10;

            var splatterView = plane.AddComponent<BloodSplatterView>();
            splatterView.Init(_splatterMaterial);
        }

        /// <summary>
        /// Отобразить отлетающий текст очков за лопнувший шар
        /// </summary>
        /// <param name="transform">Трансформация шара</param>
        /// <param name="score">Число очков</param>
        private void ShowScores(Transform transform, int score)
        {
            var go = (GameObject) GameObject.Instantiate(_scoreTextPrefab);
            go.transform.position = new Vector3(transform.position.x, transform.position.y, -101);
            go.transform.parent = gameObject.transform;
            go.AddComponent<ScoreView>().Init(score);
            //go.layer = 9;
        }
        
        /// <summary>
        /// Очистка игрового уровня
        /// </summary>
        public void Clear()
        {
            _level.NewObjectCreated -= OnNewObjectCreated;
            var views = gameObject.GetComponentsInChildren<BallView>();
            foreach (var ballView in views)
            {
                DestroyObject(ballView.gameObject);
            }
            _level = null;
            _controller = null;
        }
    }

    public interface IGameArea
    {
        float Left { get; }
        float Right { get; }
    }
}
