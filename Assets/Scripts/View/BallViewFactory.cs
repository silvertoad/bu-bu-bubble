﻿using BubblesGame.Models.Interfaces;
using UnityEngine;

namespace BubblesGame.View
{
    class BallViewFactory : IGameObjectViewFactory
    {
        private readonly GameObject _ballObject;

        /// <summary>
        /// Достать префаб игрового объекта
        /// Данные загружаются из менеджера ресурсов -> bundle
        /// </summary>
        /// <param name="resourceManager">RM</param>
        public BallViewFactory(ResourceManager resourceManager)
        {
            _ballObject = resourceManager.GetGameObject("BallObject");
        }

        /// <summary>
        /// Создать новый экземпляр игрового объекта
        /// </summary>
        /// <returns>GameObject</returns>
        public GameObject Instantiate()
        {
            return (GameObject)GameObject.Instantiate(_ballObject);
        }
    }
}
