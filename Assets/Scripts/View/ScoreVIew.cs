﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BubblesGame.Utils;
using UnityEngine;

namespace Assets.Scripts.View
{
    /// <summary>
    /// Отображние "отлетающих" очков при уничтожения шара
    /// </summary>
    class ScoreView : MonoBehaviour
    {
        private Tweener _tweener;

        public void Init(int score)
        {
            var textMesh = gameObject.GetComponent<TextMesh>();
            textMesh.text = score.ToString();

            var targetPos = gameObject.transform.position;
            targetPos.y -= 100;

            _tweener = new Tweener(gameObject.transform)
                .Add(new Tween("position", TweenType.Lerp, gameObject.transform.position, targetPos, 500))
                .Start();
            _tweener.Complete += OnTweenComplete;
        }

        public void Update()
        {
            _tweener.Update();
        }

        private Vector3 position
        {
            get { return gameObject.transform.position; }
            set { gameObject.transform.position = value; }
        }

        private void OnTweenComplete(Tweener obj)
        {
            DestroyObject(gameObject);
        }
    }
}
