using System;
using BubblesGame.Models.Interfaces;
using BubblesGame.Utils;
using UnityEngine;

namespace BubblesGame.View
{
    public class BallView : MonoBehaviour
    {
        public delegate void EventHandler(BallView ballView);

        public event EventHandler MouseDown = delegate {};

        private IGameObjectModel _model;

        private IGameArea _area;

        private Boolean destroyed = false;

        /// <summary>
        /// ������������� ������
        /// </summary>
        /// <param name="model">������ ����</param>
        /// <param name="area">������� ��� �������</param>
        /// <param name="texture">�������� ��� ������� ����</param>
        public void Init(IGameObjectModel model, IGameArea area, Texture2D texture)
        {
            _area = area;
            _model = model;
            _model.Moved += _model_Moved;
            _model.Removed += _model_Removed;

            gameObject.renderer.material.mainTexture = texture;
            _model_Moved();
        }

        /// <summary>
        /// ��������� ���� �������� �������
        /// ������� �� �������
        /// </summary>
        void _model_Removed()
        {
            destroyed = true;
            _model.Moved -= _model_Moved;
            _model.Removed -= _model_Removed;
        }

        void PlaySound()
        {
            SoundManager.PlayBallonPops(gameObject.transform.parent.gameObject.GetComponent<AudioSource>());
        }

        private Vector3 newPosition;
        private Vector3 newScale;

        /// <summary>
        /// ��������� ������ ������� � ������� ���������� �� ������
        /// �������� �������
        /// </summary>
        void FixedUpdate()
        {
            gameObject.transform.position = newPosition;
            gameObject.transform.localScale = newScale;

            if (destroyed) DestroyObject(gameObject);
        }

        /// <summary>
        /// ���������� ������ ���������������� � �������
        /// 
        /// </summary>
        private void _model_Moved()
        {
            var x0 = _area.Left*Screen.width;
            var w = Screen.width*(_area.Right - _area.Left);
            var h = Screen.height;

            newPosition = new Vector3(x0 + w * _model.X, h * _model.Y, w*_model.Z);
            newScale = Vector3.one * w * _model.Size;
        }

        /// <summary>
        /// ���� �� ������, ������������ ����� � ��������� ������� ������
        /// </summary>
        void OnMouseDown()
        {
            if (!destroyed)
            {
                PlaySound();
                MouseDown(this);
            }
        }

        public int id
        {
            get { return _model.Id; }
        }
    }
}
