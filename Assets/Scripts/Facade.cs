﻿using System;
using Assets.Scripts.network;
using Assets.Scripts.network.data;

namespace Assets.Scripts
{
    /// <summary>
    /// Фасад приложения
    /// </summary>
    internal class Facade
    {
        private static readonly Facade Instance;

        static Facade()
        {
            Instance = new Facade();
        }

        public static Facade I
        {
            get { return Instance; }
        }

        private const Int16 DefaultPort = 8888;

        private UserInfo _user;

        private Inbox _inbox;
        
        private Network _network;
        
        /// <summary>
        /// Инициализатор
        /// </summary>
        /// <param name="userName">имя игрока</param>
        /// <param name="seed">сид игрока (рандомный ключ для генерации уровней)</param>
        public void Init(string userName, Int32 seed)
        {
            _network = new Network(DefaultPort);
            _user = new UserInfo(userName, seed);
            _inbox = new Inbox();
        }
        
        /// <summary>
        /// Инфо основного игрока
        /// </summary>
        public UserInfo User
        {
            get { return _user; }
        }

        /// <summary>
        /// Гейт в сеть
        /// </summary>
        public Network Net
        {
            get { return _network; }
        }

        /// <summary>
        /// Инбокс
        /// </summary>
        public Inbox Inbox
        {
            get { return _inbox; }
        }
    }
}