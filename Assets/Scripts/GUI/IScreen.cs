﻿using System;
using System.Collections;
using UnityEngine;

namespace BubblesGame.GUI
{
    /// <summary>
    /// Возможные типы игровых экранов
    /// </summary>
    enum ScreenType
    {
        Login,  //Первое окно - ввод имени игрока
        MainMenu, //Основное игровое меню
        SinglePlayer, //Экран одиночной игры
        FinishSinglePlayer, //Экран завершения одиночной игры
        NetworkMenu, //Экран настроек сетевой игры
        OutboxConnection, //Экран ожидания ответа на приглашение в сетевую игру
        Inbox, //Инбокс (инвайты в сетевую игру)
        MultiPlayer, //Экран многопользовательской игры
        FinishMultiPlayer, //Экран завершения многопользовательской игры
        ExitGame //Экран выхода из игры
    }
    /// <summary>
    /// Класс для евента смены одного экрана на другой
    /// </summary>
    class ScreenExitEvent
    {
        /// <summary>
        /// Тип нового экрана
        /// </summary>
        public ScreenType Type { get; private set; }
        
        /// <summary>
        /// Параметры передающиеся в IScreen::Init() нового экрана
        /// </summary>
        public Hashtable Param { get; private set; }

        public ScreenExitEvent(ScreenType type, Hashtable param = null)
        {
            Param = param;
            Type = type;
        }
    }
    
    /// <summary>
    /// Интерфейс сущности игрового экрана
    /// </summary>
    interface IScreen
    {
        /// <summary>
        /// Событие выхода из экрана и перехода в другой экран
        /// </summary>
        event Action<ScreenExitEvent> Exit;

        /// <summary>
        /// Инициализация экрана
        /// </summary>
        /// <param name="param">параметры экрана</param>
        void Init(Hashtable param);
    }
}