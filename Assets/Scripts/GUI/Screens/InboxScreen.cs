﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using Assets.Scripts.GUI.Components;
using Assets.Scripts.network;
using Assets.Scripts.network.data;
using BubblesGame.GUI;
using BubblesGame.GUI.Components;
using UnityEngine;
using BubblesGame.Utils;

namespace BubblesGame.GUI.Screens
{
    /// <summary>
    /// Экран входящих сообщений
    /// </summary>
    class InboxScreen : IScreen
    {
        ///<summary>Событие выхода из экрана</summary>
        public event Action<ScreenExitEvent> Exit;
        
        private GameObject _go;
        private ScreenExitEvent _exitEvent;
        private InboxBehaviour _screen;
        private IUserModel user;

        /// <summary>
        /// Инициализатор экрана сообщений
        /// </summary>
        /// <param name="param">Параметры инициализации</param>
        public void Init(Hashtable param)
        {
            _go = new GameObject();
            _screen = _go.AddComponent<InboxBehaviour>();
            _screen.Close += OnClose;
            _screen.AcceptInvite += OnAcceptInvite;
            _screen.RejectInvite += OnRejectInvite;

            OnInboxUpdate();
            Facade.I.Inbox.Update += OnInboxUpdate;
        }

        // Обработчик обновления инбокса
        private void OnInboxUpdate()
        {
            _screen.InboxUpdate(Facade.I.Inbox.GetMessages());
        }

        // Обработчик отказа от приглашения в игру
        private void OnRejectInvite(IUserModel user)
        {
            Facade.I.Net.ResolveInvite(false);
        }

        // Обработчик подтверждения приглашения в игру
        private void OnAcceptInvite(IUserModel user)
        {
            this.user = user;
            Facade.I.Net.OnAcceptInviteComplete += OnInviteConfirmed;
            Facade.I.Net.ResolveInvite(true);
        }

        // Обработчик завершения подтверждения приглашения в игру
        private void OnInviteConfirmed(GameBroadcaster broadcaster)
        {
            Facade.I.Net.OnAcceptInviteComplete -= OnInviteConfirmed;

            Hashtable param = new Hashtable();
            param["player"] = user;
            param["broadcaster"] = broadcaster;
            _exitEvent = new ScreenExitEvent(ScreenType.MultiPlayer, param);
            _screen.Update += ExitOnNextUpdate;
        }

        // Обработчик закрытия окна в следующем кадре
        private void ExitOnNextUpdate()
        {
            _screen.Update -= ExitOnNextUpdate;
            ExitTo(_exitEvent);
        }

        // Обработчик загрытия экрана из компонента
        private void OnClose()
        {
            ExitTo(new ScreenExitEvent(ScreenType.MainMenu));
        }

        // Завершение экрана
        private void ExitTo(ScreenExitEvent e)
        {
            Facade.I.Inbox.Update -= OnInboxUpdate;

            _screen.Close -= OnClose;
            _screen.AcceptInvite -= OnAcceptInvite;
            _screen.RejectInvite -= OnRejectInvite;

            GameObject.DestroyObject(_go);
            Exit(e);
        }
    }

    /// <summary>
    /// Рендерер экрана входящих сообщений
    /// </summary>
    internal sealed class InboxBehaviour : MonoBehaviour
    {
        ///<summary>Событие закрытия окна</summary>
        public event Action Close = delegate { };
        ///<summary>Событие подтверждения приглашения в игру</summary>
        public event Action<IUserModel> AcceptInvite = delegate { };
        ///<summary>Событие отклонения приглашения в игру</summary>
        public event Action<IUserModel> RejectInvite = delegate { };
        ///<summary>Событие обновления сцены</summary>
        public event Action Update = delegate { };

        private static Vector2 NormalCloseBtnSize = new Vector2(125, 60);
        private static Vector2 MaxCloseBtnSize = new Vector2(140, 80);
        
        private ButtonRenderer closeButton;
        private CenteredLabelRenderer inboxInfo;

        private GUISkin guiSkin = (GUISkin)AssetManager.GetAsset("MainSkin");

        private List<InboxMessageRenderer> items = new List<InboxMessageRenderer>();

        private Vector2 windowSize;

        void Start()
        {
            windowSize = new Vector2(370, 330);

            closeButton = new ButtonRenderer("Back", new Rect(0, 0, MaxCloseBtnSize.x, MaxCloseBtnSize.y), NormalCloseBtnSize, true)
                .AddTween(new Tween("Size", TweenType.Lerp, NormalCloseBtnSize, MaxCloseBtnSize, 80))
                .AddTween(new Tween("Size", TweenType.Lerp, MaxCloseBtnSize, NormalCloseBtnSize, 100, 90));
            closeButton.OnClick += OnCloseClick;

            inboxInfo = new CenteredLabelRenderer("There are no messages!", new Vector2(windowSize.x - 90, 50), guiSkin);
        }

        ///<summary>Обработка обновления инбокса</summary>
        public void InboxUpdate(IList<InboxMessage> messages)
        {
            ClearInbox();
            items.Clear();
            for (int i = 0; i < messages.Count; i++)
            {
                AddItem(messages[i]);
            }
        }

        private void AddItem(InboxMessage message)
        {
            if (message is InboxConnection)
            {
                var item = new InboxConnectionRenderer(message, guiSkin);
                item.Message.OnConfirm += OnConfirm;
                items.Add(item);
            }
        }

        private void ClearInbox()
        {
            List<InboxMessageRenderer> list = items;
            if (list != null && list.Count > 0)
            {
                list.ForEach((item) => item.Message.OnConfirm -= OnConfirm);
            }
        }

        // Подтвержение или отказ от приглащения в игру
        private void OnConfirm(InboxMessage message)
        {
            if (message is InboxConnection)
            {
                if (message.IsAccepted)
                {
                    AcceptInvite(message.Sender);
                }
                else  
                {
                    RejectInvite(message.Sender);
                }
            }
        }

        void OnGUI()
        {
            UnityEngine.GUI.skin = guiSkin;

            GUILayout.Window(0, WindowLayout.GetCenteredRect(windowSize), UpdateWindow, "Inbox");

            Update();
        }

        // Отрисовка окна
        private void UpdateWindow(int windowId)
        {
            DrawInboxMessages();
            GUILayout.Space(1);
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            closeButton.Draw();
            GUILayout.EndHorizontal();
        }

        // Отрисовка сообщений в инбоксе
        private float DrawInboxMessages()
        {
            List<InboxMessageRenderer> list = items;
            if (list.Count == 0)
            {
                inboxInfo.Draw();
                return 50;
            }
            float height = 0;
            list.ForEach((item) => {
                item.Draw();
                height += item.Size.y;
            });
            return height;
        }

        private void OnCloseClick(ButtonRenderer button)
        {
            Close();
        }
    }
}
