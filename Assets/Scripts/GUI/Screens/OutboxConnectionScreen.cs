﻿using System;
using System.Collections;
using Assets.Scripts;
using Assets.Scripts.network;
using Assets.Scripts.network.data;
using BubblesGame.GUI.Components;
using UnityEngine;
using BubblesGame.Utils;

namespace BubblesGame.GUI.Screens
{
    /// <summary>
    /// Экран подтвержения сетевой игры
    /// </summary>
    class OutboxConnectionScreen : IScreen
    {
        ///<summary>Событие выхода из экрана</summary>
        public event Action<ScreenExitEvent> Exit;
        
        private GameObject _go;
        
        private ScreenExitEvent _exitEvent;
        
        private OutboxConnectionBehaviour _screen;

        /// <summary>
        /// Инициализатор экрана подтвержения сетевой игры
        /// </summary>
        /// <param name="param">Параметры инициализации</param>
        public void Init(Hashtable param)
        {
            string host = (string)param["host"];
            short port = (short)param["port"];
            Facade.I.Net.ConnectTo(host, port);
            Facade.I.Net.OnInviteAccepted += OnInviteAccepted;
            Facade.I.Net.OnInviteReject += OnInviteRejected;
            Facade.I.Net.OnConnectFail += OnConnectionFail;

            _go = new GameObject();
            _screen = _go.AddComponent<OutboxConnectionBehaviour>();
            _screen.Cancel += OnCancel;
        }

        // Обработчик подтверждения начала игры удаленным пользователем
        private void OnInviteAccepted(GameBroadcaster broadcaster, IUserModel user)
        {
            var hashtable = new Hashtable();
            hashtable["broadcaster"] = broadcaster;
            hashtable["player"] = user;
            _exitEvent = new ScreenExitEvent(ScreenType.MultiPlayer, hashtable);
            _screen.Update += ExitOnNextUpdate;
        }

        // Обработчик отказа от сетевой игры удаленным пользователем
        private void OnInviteRejected(IUserModel user)
        {
            _screen.OnConnectError(new ConnectionInfo().Reject(user));
        }

        // Обработчик ошибки подключения
        private void OnConnectionFail()
        {
            _screen.OnConnectError(new ConnectionInfo().Fail());
        }

        // Выход из экрана в следующем кадре
        private void ExitOnNextUpdate()
        {
            _screen.Update -= ExitOnNextUpdate;
            ExitTo(_exitEvent);
        }

        // Обработчик выхода из экрана при нажатии на кнопку
        private void OnCancel()
        {
            Facade.I.Net.Disconnect();
            ExitTo(new ScreenExitEvent(ScreenType.NetworkMenu));
        }

        // Завершение экрана
        private void ExitTo(ScreenExitEvent e)
        {
            Facade.I.Net.OnInviteAccepted -= OnInviteAccepted;
            Facade.I.Net.OnInviteReject -= OnInviteRejected;
            Facade.I.Net.OnConnectFail -= OnConnectionFail;
            
            _screen.Cancel -= OnCancel;
            GameObject.DestroyObject(_go);
            Exit(e);
        }
    }

    /// <summary>
    /// Рендерер экрана подтвержения сетевой игры
    /// </summary>
    internal sealed class OutboxConnectionBehaviour : MonoBehaviour
    {
        ///<summary>Событие отмены подключения и выхода из окна</summary>
        public event Action Cancel = delegate { };
        ///<summary>Событие обновления окна</summary>
        public event Action Update = delegate { };

        private InfoWindowRenderer window;

        private GUISkin guiSkin;

        void Start()
        {
            guiSkin = (GUISkin)AssetManager.GetAsset("MainSkin");

            window = new InfoWindowRenderer(guiSkin, "Connect", "Wait for user response", "Cancel");
            window.Confirm += OnCancel;
        }

        void OnGUI()
        {
            UnityEngine.GUI.skin = guiSkin;

            window.Draw();
            
            Update();
        }

        ///<summary>Обработчик ошибки соединения</summary>
        public void OnConnectError(ConnectionInfo error)
        {
            if (error.Failed)
            {
                window.Text = "Connection has failed!";
            }
            else if (error.RejectedBy != null)
            {
                window.Text = error.RejectedBy.Name + " has refused!";
            }
            window.ButtonLabel = "Go Back";
        }

        private void OnCancel()
        {
            window.Confirm -= OnCancel;
            Cancel();
        }
    }

    /// <summary>
    /// Информация о подключении
    /// </summary>
    internal class ConnectionInfo
    {
        public IUserModel RejectedBy { get; private set; }
        public bool Failed { get; private set; }

        public ConnectionInfo Fail()
        {
            Failed = true;
            return this;
        }

        public ConnectionInfo Reject(IUserModel user)
        {
            RejectedBy = user;
            return this;
        }
    }
}
