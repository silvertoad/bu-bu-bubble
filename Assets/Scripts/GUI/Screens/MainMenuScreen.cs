using System;
using System.Collections;
using Assets.Scripts;
using Assets.Scripts.GUI.Components;
using Assets.Scripts.network;
using Assets.Scripts.network.data;
using UnityEngine;
using BubblesGame.Utils;
using BubblesGame.GUI.Components;
using BubblesGame.Models.Interfaces;

namespace BubblesGame.GUI.Screens
{
    /// <summary>
    /// Экран главного меню
    /// </summary>
    class MainMenuScreen : IScreen
    {
        ///<summary>Событие завершения экрана</summary>
        public event Action<ScreenExitEvent> Exit;
        
        private GameObject _container;
        
        private MainMenuBehaviour _screen;

        /// <summary>
        /// Инициализатор экрана главного меню
        /// </summary>
        /// <param name="param">Параметры инициализации</param>
        public void Init(Hashtable param)
        {
            _container = new GameObject();
            _screen = _container.AddComponent<MainMenuBehaviour>();
            _screen.NewGame += OnNewGameClick;
            _screen.NetworkGame += OnNetworkGameClick;
            _screen.Exit += OnExitClick;
            _screen.Inbox += OnInboxClick;

            OnInboxUpdate();
            Facade.I.Inbox.Update += OnInboxUpdate;
        }

        // Обработчик обновления инбокса
        private void OnInboxUpdate()
        {
            _screen.UpdateInboxMessages(Facade.I.Inbox.Count);
        }

        // Обработчик клика по кнопке запуска новой одиночной игры
        private void OnNewGameClick(ButtonRenderer button)
        {
            ExitTo(new ScreenExitEvent(ScreenType.SinglePlayer));
        }

        // Обработчик клика по кнопке перехода в сетевое меню
        private void OnNetworkGameClick(ButtonRenderer button)
        {
            ExitTo(new ScreenExitEvent(ScreenType.NetworkMenu));
        }

        // Обработчик клика по кнопке выхода из игры
        private void OnExitClick(ButtonRenderer button)
        {
            ExitTo(new ScreenExitEvent(ScreenType.ExitGame));
        }

        // Обработчик клика по кнопке в инбокс
        private void OnInboxClick()
        {
            ExitTo(new ScreenExitEvent(ScreenType.Inbox));
        }

        // Переход в новый экран
        private void ExitTo(ScreenExitEvent obj)
        {
            _screen.NewGame -= OnNewGameClick;
            _screen.NetworkGame -= OnNetworkGameClick;
            _screen.Exit -= OnExitClick;
            _screen.Inbox += OnInboxClick;

            Facade.I.Inbox.Update -= OnInboxUpdate;

            GameObject.DestroyObject(_container);
            Exit(obj);
        }
    }

    /// <summary>
    /// Рендерер экрана главного меню
    /// </summary>
    internal sealed class MainMenuBehaviour : MonoBehaviour
    {
        ///<summary>Событие клика по кнопке создания новой одиночной игры</summary>
        public event Action<ButtonRenderer> NewGame;
        ///<summary>Событие клика по кнопке перехода в сетевое меню</summary>
        public event Action<ButtonRenderer> NetworkGame;
        ///<summary>Событие клика по кнопке выхода из игры</summary>
        public event Action<ButtonRenderer> Exit;
        ///<summary>Событие клика по кнопке перехода в инбокс</summary>
        public event Action Inbox;
        ///<summary>Событие обновления окна</summary>
        public event Action Update = delegate { };

        private static Vector2 NormalBtnSize = new Vector2(260, 67);
        private static Vector2 MaxBtnSize = new Vector2(285, 73);

        private GUISkin guiSkin;

        private ButtonRenderer newGameButton;
        private ButtonRenderer connectButton;
        private ButtonRenderer exitButton;
        private InboxButtonRenderer inboxButton;

        private int numMessages = 0;

        void Start()
        {
            guiSkin = (GUISkin)AssetManager.GetAsset("MainSkin");

            newGameButton = new ButtonRenderer("New Game", new Rect(40, 100, 285, 75), NormalBtnSize)
                .AddTween(new Tween("Size", TweenType.Lerp, NormalBtnSize, MaxBtnSize, 200 + 80))
                .AddTween(new Tween("Size", TweenType.Lerp, MaxBtnSize, NormalBtnSize, 80, 200 + 85));
            newGameButton.OnClick += NewGame;

            connectButton = new ButtonRenderer("Connect To", new Rect(40, 170, 285, 75), NormalBtnSize)
                .AddTween(new Tween("Size", TweenType.Lerp, NormalBtnSize, MaxBtnSize, 100 + 65))
                .AddTween(new Tween("Size", TweenType.Lerp, MaxBtnSize, NormalBtnSize, 100, 100 + 70));
            connectButton.OnClick += NetworkGame;

            exitButton = new ButtonRenderer("Exit", new Rect(40, 240, 285, 75), NormalBtnSize)
                .AddTween(new Tween("Size", TweenType.Lerp, NormalBtnSize, MaxBtnSize, 50))
                .AddTween(new Tween("Size", TweenType.Lerp, MaxBtnSize, NormalBtnSize, 120, 50));
            exitButton.OnClick += Exit;

            inboxButton = new InboxButtonRenderer(guiSkin);
            inboxButton.NumMessages = numMessages;
            inboxButton.Click += Inbox;
        }

        // Обработчик обновления количества сообщений в инбоксе
        public void UpdateInboxMessages(int numMessages)
        {
            this.numMessages = numMessages;
            if (inboxButton != null)
                inboxButton.NumMessages = numMessages;
        }

        void OnGUI()
        {
            UnityEngine.GUI.skin = guiSkin;

            Rect windowBounds = WindowLayout.GetCenteredRect(370, 330);
            GUILayout.Window(0, windowBounds, UpdateWindowLayout, "Main menu");

            GUILayout.BeginHorizontal(GUILayout.Width(Screen.width));
            inboxButton.Draw();
            GUILayout.FlexibleSpace();
            GUILayout.Label("Your address:\n" + Facade.I.Net.Addres, guiSkin.GetStyle("CenteredLabel"));
            GUILayout.EndHorizontal();

            Update();
        }

        // Отрисовка окна меню
        private void UpdateWindowLayout(int windowId)
        {
            GUILayout.BeginVertical();
            newGameButton.Draw();
            connectButton.Draw();
            exitButton.Draw();
            GUILayout.BeginVertical();
        }
    }
}
