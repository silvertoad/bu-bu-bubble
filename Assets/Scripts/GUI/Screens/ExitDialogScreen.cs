﻿using System;
using System.Collections;
using Assets.Scripts.GUI.Components;
using BubblesGame.GUI.Components;
using UnityEngine;
using BubblesGame.Utils;

namespace BubblesGame.GUI.Screens
{
    /// <summary>
    /// Экран выхода из игры
    /// </summary>
    class ExitDialogScreen : IScreen
    {
        private GameObject _container;

        ///<summary>Событие выхода из экрана</summary>
        public event Action<ScreenExitEvent> Exit;

        /// <summary>
        /// Инициализатор экрана выхода из игры
        /// </summary>
        /// <param name="param">Параметры инициализации</param>
        public void Init(Hashtable param)
        {
            _container = new GameObject();
            var behaviour = _container.AddComponent<ExitDialogBehaviour>();
            behaviour.Exit += behaviour_Exit;
        }

        private void behaviour_Exit(ScreenExitEvent obj)
        {
            GameObject.Destroy(_container);
            Exit(obj);
        }
    }

    /// <summary>
    /// Рендерер экрана выхода из игры
    /// </summary>
    internal sealed class ExitDialogBehaviour : MonoBehaviour
    {
        ///<summary>Событие закрытия окна</summary>
        public event Action<ScreenExitEvent> Exit;

        private GUISkin skin;

        private static Vector2 NormalExitBtnSize = new Vector2(170, 65);
        private static Vector2 NormalBackBtnSize = new Vector2(140, 65);
        private static Vector2 MaxExitBtnSize = new Vector2(185, 75);
        private static Vector2 MaxBackBtnSize = new Vector2(155, 75);

        private ButtonRenderer backButton;
        private ButtonRenderer exitButton;

        void Start()
        {
            skin = (GUISkin)AssetManager.GetAsset("MainSkin");

            backButton = new ButtonRenderer("No", new Rect(40, 90, 155, 80), NormalBackBtnSize)
                .AddTween(new Tween("Size", TweenType.Lerp, NormalBackBtnSize, MaxBackBtnSize, 90))
                .AddTween(new Tween("Size", TweenType.Lerp, MaxBackBtnSize, NormalBackBtnSize, 190, 90));
            backButton.OnClick += OnBackButton;

            exitButton = new ButtonRenderer("Yes", new Rect(190, 90, 185, 80), NormalExitBtnSize)
                .AddTween(new Tween("Size", TweenType.Lerp, NormalExitBtnSize, MaxExitBtnSize, 80, 90))
                .AddTween(new Tween("Size", TweenType.Lerp, MaxExitBtnSize, NormalExitBtnSize, 150, 170));
            exitButton.OnClick += OnExitButton;
        }

        void OnGUI()
        {
            UnityEngine.GUI.skin = skin;

            var windowBounds = WindowLayout.GetCenteredRect(420, 210);
            
            GUILayout.Window(0, windowBounds, UpdateWindow, "Exit");
        }

        private void UpdateWindow(int windowId)
        {
            GUILayout.BeginHorizontal(GUILayout.Width(305), GUILayout.Height(80));
            backButton.Draw();
            exitButton.Draw();
            GUILayout.EndHorizontal();
        }

        private void OnBackButton(ButtonRenderer button)
        {
            Exit(new ScreenExitEvent(ScreenType.MainMenu));
            backButton.Dispose();
            exitButton.Dispose();
        }

        private void OnExitButton(ButtonRenderer button)
        {
            Application.Quit();
        }
    }
}
