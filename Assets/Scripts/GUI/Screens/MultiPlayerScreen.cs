﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using Assets.Scripts.network;
using Assets.Scripts.network.data;
using BubblesGame;
using BubblesGame.Models;
using BubblesGame.Models.Game.LevelGeneration;
using BubblesGame.Models.Interfaces;
using BubblesGame.Models.Player;
using BubblesGame.Utils;
using UnityEngine;
using Object = UnityEngine.Object;

namespace BubblesGame.GUI.Screens
{
    /// <summary>
    /// Скрин сетевой игры
    /// </summary>
    class MultiPlayerScreen : IScreen
    {
        public event Action<ScreenExitEvent> Exit;

        private GameObject _container;

        private PlayerModel _playerRight;
        private PlayerModel _playerLeft;
        private MultiplayerLevelConroller _controllerLeft;
        private MultiplayerLevelConroller _controllerRight;
        private GameManager _managerRight;
        private GameManager _managerLeft;
        private GameBroadcaster _broadCaster;

        /// <summary>
        /// Инициализатор
        /// </summary>
        /// <param name="param"></param>
        public void Init(Hashtable param)
        {
            _broadCaster = (GameBroadcaster) param["broadcaster"];
            var opponentInfo = (UserInfo) param["player"];

            //вычисляем сид как сумму сидов двух игроков
            var levelSeed = opponentInfo.Seed + Facade.I.User.Seed;

            _playerLeft = new PlayerModel(Facade.I.User);
            _playerRight = new PlayerModel(opponentInfo);

            //создаем менеджер и контроллер для противника
            _controllerRight = new OpponentLevelController(_playerRight, _broadCaster);
            _managerRight = new GameManager(_controllerRight);
            _managerRight.LevelView.SetSpace(0f, 0.5f);

            //... и для себя
            _controllerLeft = new PlayerLevelController(_playerLeft, _broadCaster);
            _managerLeft = new GameManager(_controllerLeft);
            _managerLeft.LevelView.SetSpace(0.5f, 1f);
            
            _container = new GameObject();
            var behaviour = _container.AddComponent<MultiPlayerBehaviour>();
            behaviour.Init(_playerLeft, _playerRight);

            _managerRight.GameOver += OnGameOver;
            _managerRight.Start(levelSeed);

            _managerLeft.GameOver += OnGameOver;
            _managerLeft.Start(levelSeed);
        }

        /// <summary>
        /// Обработчик завершения игры
        /// </summary>
        private void OnGameOver()
        {
            _managerRight.GameOver -= OnGameOver;
            _managerLeft.GameOver -= OnGameOver;
            _managerLeft.Destroy();
            _managerRight.Destroy();
            
            IPlayerModel winner;
            IPlayerModel loser;

            //определяем победителя и проигравшего
            if (_playerLeft.HP > _playerRight.HP)
            {
                winner = _playerLeft;
                loser = _playerRight;
                _controllerLeft.EndGameExternally();
            }
            else
            {
                winner = _playerRight;
                loser = _playerLeft;
                _controllerRight.EndGameExternally();
            }
            
            //закрываем сокеты
            if(_broadCaster.QueueCount > 0)
                _broadCaster.QueueComplete += OnBroadcasterQueueComplete;
            else
                Facade.I.Net.Disconnect();

            //очищаем экран
            _container.GetComponent<MultiPlayerBehaviour>().Destroy();
            GameObject.Destroy(_container);

            //переходим в окно завершения игры
            Hashtable param = new Hashtable();
            param["winner"] = winner;
            param["loser"] = loser;
            Exit(new ScreenExitEvent(ScreenType.FinishMultiPlayer, param));    
        }

        private void OnBroadcasterQueueComplete()
        {
            _broadCaster.QueueComplete -= OnBroadcasterQueueComplete;
            Facade.I.Net.Disconnect();
        }
    }

    /// <summary>
    /// Бихевиор отображения параметров двух игроков одновременно
    /// </summary>
    internal class MultiPlayerBehaviour : MonoBehaviour
    {
        private IPlayerModel _playerLeft;
        private IPlayerModel _playerRight;

        private GameManager _manager;

        private GUISkin _guiSkin;

        const int BounceSpeed = 4;
        const int MaxElementSize = 50;
        const int MinElementSize = 42;

        private float _scoreSizeLeft;
        private float _hpSizeLeft;
        private float _scoreSizeRight;
        private float _hpSizeRight;

        private LineRenderer _lineRenderer;

        private Rect _namesArea = new Rect(0, 10, Screen.width, 50);
        private Rect _scoresArea = new Rect(0, 60, Screen.width, 50);
        private Rect _hpArea = new Rect(0, 110, Screen.width, 50);

        /// <summary>
        /// Инициализатор
        /// </summary>
        /// <param name="playerLeft"></param>
        /// <param name="playerRight"></param>
        public void Init(IPlayerModel playerLeft, IPlayerModel playerRight)
        {
            _playerLeft = playerLeft;
            _playerLeft.ScoreChanged += OnScoreLeftChanged;
            _playerLeft.HPChanged += OnHPLeftChanged;

            _playerRight = playerRight;
            _playerRight.ScoreChanged += OnScoreRightChanged;
            _playerRight.HPChanged += OnHPRightChanged;
        }

        void Start()
        {
            _guiSkin = (GUISkin)AssetManager.GetAsset("MainSkin");

            //вертикальная разделяющая линия
            _lineRenderer = gameObject.AddComponent<LineRenderer>();
            _lineRenderer.SetVertexCount(2);
            _lineRenderer.material = new Material(Shader.Find("Diffuse"));
            _lineRenderer.SetWidth(10, 10);
        }

        void OnGUI()
        {
            UnityEngine.GUI.skin = _guiSkin;

            float deltaTime = Time.deltaTime;

            GUIStyle scoreStyle = _guiSkin.GetStyle("ScoreBar");
            GUIStyle hpStyle = _guiSkin.GetStyle("HPBar");
            GUIStyle nameStyle = _guiSkin.GetStyle("CenteredLabel");

            GUILayout.BeginArea(_namesArea);
                GUILayout.BeginHorizontal(GUILayout.Width(_namesArea.width));
                    GUILayout.Label(_playerLeft.User.Name, nameStyle);
                    GUILayout.FlexibleSpace();
                    GUILayout.Label(_playerRight.User.Name, nameStyle);    
                GUILayout.EndHorizontal();
            GUILayout.EndArea();

            GUILayout.BeginArea(_scoresArea);
                GUILayout.BeginHorizontal(GUILayout.Width(_scoresArea.width));
                    UpdateLeft(deltaTime, "Score: " + _playerLeft.Score.ToString(), scoreStyle, ref _scoreSizeLeft);
                    GUILayout.FlexibleSpace();
                    UpdateRight(deltaTime, "Score: " + _playerRight.Score.ToString(), scoreStyle, ref _scoreSizeRight);
                GUILayout.EndHorizontal();
            GUILayout.EndArea();

            GUILayout.BeginArea(_hpArea);
                GUILayout.BeginHorizontal(GUILayout.Width(_hpArea.width));
                    UpdateLeft(deltaTime, "Lives: " + _playerLeft.HP.ToString(), hpStyle, ref _hpSizeLeft);
                    GUILayout.FlexibleSpace();
                    UpdateRight(deltaTime, "Lives: " + _playerRight.HP.ToString(), hpStyle, ref _hpSizeRight);
                GUILayout.EndHorizontal();
            GUILayout.EndArea();
        }

        void Update()
        {
            int w = Screen.width / 2;
            int h = Screen.height;
            _lineRenderer.SetPosition(0, new Vector3(w, h));
            _lineRenderer.SetPosition(1, new Vector3(w, 0));
        }

        void OnScoreLeftChanged() { _scoreSizeLeft = MaxElementSize; }
        void OnScoreRightChanged() { _scoreSizeRight = MaxElementSize; }

        void OnHPLeftChanged() { _hpSizeLeft = MaxElementSize; }
        void OnHPRightChanged() { _hpSizeRight = MaxElementSize; }

        void UpdateLeft(float deltaTime, string text, GUIStyle style, ref float size)
        {
            GUILayout.Label(text, style, GUILayout.Width(size), GUILayout.Height(size));
            size = Mathf.Lerp(size, MinElementSize, BounceSpeed * deltaTime);
        }

        void UpdateRight(float deltaTime, string text, GUIStyle style, ref float size)
        {
            GUILayout.Label(text, style, GUILayout.Width(size), GUILayout.Height(size));
            size = Mathf.Lerp(size, MinElementSize, BounceSpeed * deltaTime);
            float textSize = style.CalcSize(new GUIContent(text)).x;
            GUILayout.Space(textSize - size + 55);
        }

        public void Destroy()
        {
            _playerLeft.ScoreChanged -= OnScoreLeftChanged;
            _playerLeft.HPChanged -= OnHPLeftChanged;
            _playerLeft = null;

            _playerRight.ScoreChanged -= OnScoreRightChanged;
            _playerRight.HPChanged -= OnHPRightChanged;
            _playerRight = null;

            _guiSkin = null;
        }
    }
}
