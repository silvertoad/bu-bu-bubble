﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.GUI.Components;
using BubblesGame.GUI;
using BubblesGame.GUI.Components;
using BubblesGame.Models.Interfaces;
using UnityEngine;
using BubblesGame.Utils;
using BubblesGame.Models;

namespace BubblesGame.GUI.Screens
{
    /// <summary>
    /// Экран завершения одиночной игры
    /// </summary>
    class FinishSingleplayerGameScreen : IScreen
    {
        ///<summary>Событие завершения экрана</summary>
        public event Action<ScreenExitEvent> Exit;
        
        private GameObject _container;

        private FinishSingleplayerGameBehaviour _screen;

        /// <summary>
        /// Инициализатор экрана завершения одиночной игры
        /// </summary>
        /// <param name="param">Параметры инициализации</param>
        public void Init(Hashtable param)
        {
            _container = new GameObject();

            IPlayerModel player = (IPlayerModel) param["player"];

            _screen = _container.AddComponent<FinishSingleplayerGameBehaviour>();
            _screen.Init(player);
            _screen.Exit += _screen_Exit;
        }

        private void _screen_Exit(ScreenExitEvent obj)
        {
            _screen.Exit -= _screen_Exit;
            GameObject.Destroy(_container);
            Exit(obj);
        }
    }

    /// <summary>
    /// Рендерер экрана завершения сетевой игры
    /// </summary>
    internal sealed class FinishSingleplayerGameBehaviour : MonoBehaviour
    {
        ///<summary>Событие закрытия окна</summary>
        public event Action<ScreenExitEvent> Exit;

        private GUISkin _guiSkin;
        private IPlayerModel _player;

        private static Vector2 NormalContinueBtnSize = new Vector2(150, 70);
        private static Vector2 MaxContinueBtnSize = new Vector2(165, 90);
        private static Vector2 NormalMenuBtnSize = new Vector2(140, 70);
        private static Vector2 MaxMenuBtnSize = new Vector2(155, 90);

        private ButtonRenderer continueButton;
        private ButtonRenderer mainMenuButton;

        /// <summary>
        /// Инициализатор окна завершения многопользовательской игры
        /// </summary>
        /// <param name="player">Текущий игрок</param>
        public void Init(IPlayerModel player)
        {
            _player = player;
            _guiSkin = (GUISkin)AssetManager.GetAsset("MainSkin");
        }

        void Start()
        {
            continueButton = new ButtonRenderer("Again", new Rect(40, 140, 160, 90), NormalContinueBtnSize)
                .AddTween(new Tween("Size", TweenType.Lerp, NormalContinueBtnSize, MaxContinueBtnSize, 100, 20))
                .AddTween(new Tween("Size", TweenType.Lerp, MaxContinueBtnSize, NormalContinueBtnSize, 150, 130));
            continueButton.OnClick += OnContinueClick;

            mainMenuButton = new ButtonRenderer("Menu", new Rect(195, 140, 150, 90), NormalMenuBtnSize)
                .AddTween(new Tween("Size", TweenType.Lerp, NormalMenuBtnSize, MaxMenuBtnSize, 80, 130))
                .AddTween(new Tween("Size", TweenType.Lerp, MaxMenuBtnSize, NormalMenuBtnSize, 200, 220));
            mainMenuButton.OnClick += OnMenuClick;
        }

        void OnGUI()
        {
            UnityEngine.GUI.skin = _guiSkin;

            GUILayout.Window(0, WindowLayout.GetCenteredRect(370, 330), UpdateDialog, "Game Over");
        }

        // Отрисовка окна завершения одиночной игры
        private void UpdateDialog(int windowId)
        {
            GUILayout.BeginHorizontal();
                GUILayout.Space(20);
                GUILayout.Label("Score: " + _player.Score, _guiSkin.GetStyle("ScoreBar"),
                    GUILayout.Width(42), GUILayout.Height(42));
                GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
                continueButton.Draw();
                mainMenuButton.Draw();
            GUILayout.EndHorizontal();
        }

        private void OnContinueClick(ButtonRenderer button)
        {
            Exit(new ScreenExitEvent(ScreenType.SinglePlayer));
        }

        private void OnMenuClick(ButtonRenderer button)
        {
            Exit(new ScreenExitEvent(ScreenType.MainMenu));
        }
    }
}
