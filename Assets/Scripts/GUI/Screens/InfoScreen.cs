﻿using System;
using System.Collections;
using UnityEngine;
using BubblesGame.GUI.Components;
using BubblesGame.Utils;

namespace BubblesGame.GUI.Screens
{
    /// <summary>
    /// Экран с информационным окном (для отображения ошибок)
    /// </summary>
    class InfoScreen : IScreen
    {
        ///<summary>Событие завершения экрана</summary>
        public event Action<ScreenExitEvent> Exit;

        private GameObject _go;
        
        private InfoBehaviour _screen;
        
        private ScreenType exitScreen;

        /// <summary>
        /// Инициализатор экрана информационного окна
        /// </summary>
        /// <param name="param">Параметры инициализации</param>
        public void Init(Hashtable param)
        {
            string header = param.ContainsKey("header") ? (string)param["header"] : null;
            string text = param.ContainsKey("text") ? (string)param["text"] : null;
            string buttonLabel = param.ContainsKey("buttonLabel") ? (string)param["buttonLabel"] : null;
            exitScreen = (ScreenType)param["exitScreen"];

            _go = new GameObject();
            _screen = _go.AddComponent<InfoBehaviour>();
            _screen.Confirm += _screen_Confirm;
        }

        private void _screen_Confirm()
        {
            _screen.Confirm -= _screen_Confirm;
            GameObject.DestroyObject(_go);
            Exit(new ScreenExitEvent(exitScreen));
        }
    }

    /// <summary>
    /// Рендерер экрана с информационным окном
    /// </summary>
    internal class InfoBehaviour : MonoBehaviour
    {
        ///<summary>Событие подтверждения и закрытия окна</summary>
        public event Action Confirm = delegate { };
        ///<summary>Событие обновления сцены</summary>
        public event Action Update = delegate { };

        private InfoWindowRenderer window;

        private GUISkin guiSkin;

        /// <summary>
        /// Инициализатор окна завершения многопользовательской игры
        /// </summary>
        /// <param name="header">Заголовок</param>
        /// <param name="text">Текст</param>
        /// <param name="buttonLabel">Название кнопки подтверждения</param>
        public void Init(string header, string text, string buttonLabel)
        {
            guiSkin = (GUISkin)AssetManager.GetAsset("MainSkin");
            
            window = new InfoWindowRenderer(guiSkin, header, text, buttonLabel);
            window.Confirm += OnConfirm;
        }

        void OnGUI()
        {
            UnityEngine.GUI.skin = guiSkin;

            window.Draw();

            Update();
        }

        private void OnConfirm()
        {
            window.Confirm -= OnConfirm;
            Confirm();
        }
    }
}
