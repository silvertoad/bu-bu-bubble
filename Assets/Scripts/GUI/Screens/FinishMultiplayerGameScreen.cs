﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.GUI.Components;
using BubblesGame.GUI;
using BubblesGame.GUI.Components;
using BubblesGame.Models.Interfaces;
using UnityEngine;
using BubblesGame.Utils;
using BubblesGame.Models;
using Assets.Scripts;
using Assets.Scripts.network.data;

namespace BubblesGame.GUI.Screens
{
    /// <summary>
    /// Экран завершения сетевой игры
    /// </summary>
    class FinishMultiplayerGameScreen : IScreen
    {
        ///<summary>Событие завершения экрана</summary>
        public event Action<ScreenExitEvent> Exit;

        private GameObject _container;

        private FinishMultiplayerGameBehaviour _screen;

        /// <summary>
        /// Инициализатор экрана завершения сетевой игры
        /// </summary>
        /// <param name="param">Параметры инициализации</param>
        public void Init(Hashtable param)
        {
            _container = new GameObject();

            IPlayerModel winner = (IPlayerModel)param["winner"];
            IPlayerModel loser = (IPlayerModel)param["loser"];
            bool currentPlayerWins = (UserInfo)winner.User == Facade.I.User;

            _screen = _container.AddComponent<FinishMultiplayerGameBehaviour>();
            _screen.Init(winner, loser, currentPlayerWins);
            _screen.Exit += _screen_Exit;
        }

        private void _screen_Exit(ScreenExitEvent obj)
        {
            _screen.Exit -= _screen_Exit;
            GameObject.Destroy(_container);
            Exit(obj);
        }
    }

    /// <summary>
    /// Рендерер экрана завершения сетевой игры
    /// </summary>
    internal sealed class FinishMultiplayerGameBehaviour : MonoBehaviour
    {
        ///<summary>Событие закрытия окна</summary>
        public event Action<ScreenExitEvent> Exit;

        private static Vector2 NormalContinueBtnSize = new Vector2(150, 70);
        private static Vector2 MaxContinueBtnSize = new Vector2(165, 90);
        private static Vector2 NormalMenuBtnSize = new Vector2(140, 70);
        private static Vector2 MaxMenuBtnSize = new Vector2(155, 90);

        private GameManager _manager;

        private IPlayerModel _winner;
        private IPlayerModel _loser;

        private string header = "";

        private GUISkin _guiSkin;

        private ButtonRenderer continueButton;
        private ButtonRenderer mainMenuButton;

        /// <summary>
        /// Инициализатор окна завершения многопользовательской игры
        /// </summary>
        /// <param name="winner">Победивший игрок</param>
        /// <param name="loser">Проигравший игрок</param>
        /// <param name="win">Победа(true)/Поражение(false)</param>
        public void Init(IPlayerModel winner, IPlayerModel loser, bool win)
        {
            header = win ? "VICTORY" : "DEFEAT";
            _winner = winner;
            _loser = loser;
        }

        void Start()
        {
            _guiSkin = (GUISkin)AssetManager.GetAsset("MainSkin");
            
            continueButton = new ButtonRenderer("Again", new Rect(40, 180, 160, 90), NormalContinueBtnSize)
                .AddTween(new Tween("Size", TweenType.Lerp, NormalContinueBtnSize, MaxContinueBtnSize, 100, 20))
                .AddTween(new Tween("Size", TweenType.Lerp, MaxContinueBtnSize, NormalContinueBtnSize, 150, 130));
            continueButton.OnClick += OnContinueClick;

            mainMenuButton = new ButtonRenderer("Menu", new Rect(195, 180, 150, 90), NormalMenuBtnSize)
                .AddTween(new Tween("Size", TweenType.Lerp, NormalMenuBtnSize, MaxMenuBtnSize, 80, 130))
                .AddTween(new Tween("Size", TweenType.Lerp, MaxMenuBtnSize, NormalMenuBtnSize, 200, 220));
            mainMenuButton.OnClick += OnMenuClick;
        }

        void OnGUI()
        {
            UnityEngine.GUI.skin = _guiSkin;

            GUILayout.Window(0, WindowLayout.GetCenteredRect(370, 330), UpdateDialog, header);
        }

        // Отрисовка окна завершения сетевой игры
        private void UpdateDialog(int windowId)
        {
            string winnerText = _winner.User.Name + ": " + _winner.Score.ToString();
            GUILayout.Label(winnerText, _guiSkin.GetStyle("ScoreBar"),
                GUILayout.Width(42), GUILayout.Height(42));

            string loserText = _loser.User.Name + ": " + _loser.Score.ToString();
            GUILayout.Label(loserText, _guiSkin.GetStyle("ScoreBar"),
                GUILayout.Width(42), GUILayout.Height(42));
            
            //continueButton.Draw();
            mainMenuButton.Draw();
        }

        private void OnContinueClick(ButtonRenderer button)
        {
            Exit(new ScreenExitEvent(ScreenType.SinglePlayer));
        }

        private void OnMenuClick(ButtonRenderer button)
        {
            _winner.Dispose();
            _loser.Dispose();
            Exit(new ScreenExitEvent(ScreenType.MainMenu));
        }
    }
}
