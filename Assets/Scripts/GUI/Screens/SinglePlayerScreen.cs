﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using BubblesGame;
using BubblesGame.Models;
using BubblesGame.Models.Game.LevelGeneration;
using BubblesGame.Models.Interfaces;
using BubblesGame.Models.Player;
using BubblesGame.Utils;
using Assets.Scripts;
using UnityEngine;

namespace BubblesGame.GUI.Screens
{
    /// <summary>
    /// Экран одиночной игры
    /// </summary>
    class SinglePlayerScreen : IScreen
    {
        ///<summary>Событие завершения экрана</summary>
        public event Action<ScreenExitEvent> Exit;

        private GameObject _container;

        private GameManager _manager;
        
        private PlayerModel _player;

        /// <summary>
        /// Инициализатор экрана одиночной игры
        /// </summary>
        /// <param name="param">Параметры инициализации</param>
        public void Init(Hashtable param)
        {
            _player = new PlayerModel(Facade.I.User);
            _manager = new GameManager(new SinglePlayerLevelController(_player));

            _container = new GameObject();
            var behaviour = _container.AddComponent<SinglePlayerBehaviour>();
            behaviour.Init(_player);

            _manager.GameOver += manager_GameOver;
            _manager.Start((int) DateTime.Now.Ticks);
        }

        private void manager_GameOver()
        {
            _container.GetComponent<SinglePlayerBehaviour>().Destroy();
            GameObject.Destroy(_container);
            _manager.Destroy();

            Hashtable param = new Hashtable();
            param["player"] = _player;
            Exit(new ScreenExitEvent(ScreenType.FinishSinglePlayer, param));  
        }
    }

    /// <summary>
    /// Рендерер экрана одиночной игры
    /// </summary>
    internal class SinglePlayerBehaviour : MonoBehaviour
    {
        private GUISkin _guiSkin;
        
        const int BounceSpeed = 4;
        const int MaxElementSize = 50;
        const int MinElementSize = 42;

        private Rect layoutBounds;
        private float scoreSize;
        private float hpSize;

        private IPlayerModel _player;
        private LineRenderer _lineRenderer;

        private GUIStyle scoreBarStyle;
        private GUIStyle hpBarStyle;

        /// <summary>
        /// Инициализатор отображения одиночной игры
        /// </summary>
        /// <param name="player">Победивший игрок</param>
        public void Init(IPlayerModel player)
        {
            _player = player;
            player.ScoreChanged += OnScoreChanged;
            player.HPChanged += OnHPChanged;

            OnResize();
        }

        void Start()
        {
            _guiSkin = (GUISkin)AssetManager.GetAsset("MainSkin");
            scoreBarStyle = _guiSkin.GetStyle("ScoreBar");
            hpBarStyle = _guiSkin.GetStyle("HPBar");
        }

        // Обработчик ресайза окна приложения
        void OnResize()
        {
            layoutBounds = new Rect(0, 0, Screen.width, 70);
        }

        void OnGUI()
        {
            UnityEngine.GUI.skin = _guiSkin;

            float deltaTime = Time.deltaTime;

            OnResize();

            GUILayout.BeginHorizontal(GUILayout.Width(Screen.width));
                GUILayout.BeginArea(layoutBounds);
                    GUILayout.FlexibleSpace();
                    UpdateScore(deltaTime);
                    GUILayout.FlexibleSpace();
                GUILayout.EndArea();
                GUILayout.BeginArea(layoutBounds);
                    GUILayout.FlexibleSpace();
                    GUILayout.BeginHorizontal();
                        GUILayout.FlexibleSpace();
                        UpdateHP(deltaTime);
                    GUILayout.EndHorizontal();
                    GUILayout.FlexibleSpace();
                GUILayout.EndArea();
            GUILayout.EndHorizontal();
        }

        // Обработчик изменения очков игрока
        private void OnScoreChanged() { scoreSize = MaxElementSize; }

        // Обработчик изменения hp игрока
        private void OnHPChanged() { hpSize = MaxElementSize; }

        // Отрисовка очков игрока
        private void UpdateScore(float deltaTime)
        {
            scoreSize = Mathf.Lerp(scoreSize, MinElementSize, BounceSpeed * deltaTime);
            GUILayout.Label("Score: " + _player.Score, scoreBarStyle, GUILayout.Width(scoreSize), GUILayout.Height(scoreSize));
        }

        // Отрисовка hp игрока
        private void UpdateHP(float deltaTime)
        {
            string hp = "Lives: " + _player.HP;
            var labelSize = hpBarStyle.CalcSize(new GUIContent(hp));
            hpSize = Mathf.Lerp(hpSize, MinElementSize, BounceSpeed * deltaTime);
            GUILayout.Label(hp, hpBarStyle, GUILayout.Width(hpSize), GUILayout.Height(hpSize));
            GUILayout.Space(labelSize.x - hpSize + 55);
        }

        // Диструктор рендерера
        public void Destroy()
        {
            _player.ScoreChanged -= OnScoreChanged;
            _player.HPChanged -= OnHPChanged;
            
            scoreBarStyle = null;
            hpBarStyle = null;
            
            _guiSkin = null;
        }
    }
}
