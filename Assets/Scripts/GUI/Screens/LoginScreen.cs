﻿using System;
using System.Collections;
using Assets.Scripts;
using Assets.Scripts.GUI.Components;
using Assets.Scripts.network;
using Assets.Scripts.network.data;
using BubblesGame.GUI;
using BubblesGame.GUI.Components;
using UnityEngine;
using BubblesGame.Utils;

namespace BubblesGame.GUI.Screens
{
    /// <summary>
    /// Экран входа в игру
    /// </summary>
    class LoginScreen : IScreen
    {
        ///<summary>Событие завершения экрана</summary>
        public event Action<ScreenExitEvent> Exit;
        
        private GameObject _container;
        
        private LoginBehaviour _screen;

        /// <summary>
        /// Инициализатор экрана входа в игру
        /// </summary>
        /// <param name="param">Параметры инициализации</param>
        public void Init(Hashtable param)
        {
            _container = new GameObject();
            _screen = _container.AddComponent<LoginBehaviour>();
            _screen.Confirm += screen_Confirm;
            _screen.UserName = Facade.I.User.Name;
        }

        private void screen_Confirm(string userName)
        {
            GameObject.Destroy(_container);
            Facade.I.User.Name = userName;
            Exit(new ScreenExitEvent(ScreenType.MainMenu));
        }
    }

    /// <summary>
    /// Рендерер экрана входа в игру
    /// </summary>
    internal sealed class LoginBehaviour : MonoBehaviour
    {
        ///<summary>Событие подтверждения и закрытия окна</summary>
        public event Action<string> Confirm;

        private static Vector2 NormalLoginBtnSize = new Vector2(70, 55);
        private static Vector2 MaxLoginBtnSize = new Vector2(80, 60);

        private ButtonRenderer confirmButton;

        private GUISkin guiSkin;

        void Start()
        {
            guiSkin = (GUISkin)AssetManager.GetAsset("MainSkin");

            confirmButton = new ButtonRenderer("OK", new Rect(250, 100, 80, 60), NormalLoginBtnSize)
                .AddTween(new Tween("Size", TweenType.Lerp, NormalLoginBtnSize, MaxLoginBtnSize, 80, 80))
                .AddTween(new Tween("Size", TweenType.Lerp, MaxLoginBtnSize, NormalLoginBtnSize, 150, 170));
            confirmButton.OnClick += OnConfirmClick;
        }

        ///<summary>Имя пользователя, введенное в поле</summary>
        public string UserName { get; set; }

        void OnGUI()
        {
            UnityEngine.GUI.skin = guiSkin;

            GUILayout.Window(0, WindowLayout.GetCenteredRect(360, 250), UpdateDialog, "Your Name");
        }

        // Отрисовка окна входа в игру
        private void UpdateDialog(int windowId)
        {
            GUILayout.BeginHorizontal();
            GUILayout.BeginVertical(GUILayout.Height(55));
            GUILayout.FlexibleSpace();
            var name = GUILayout.TextField(UserName, guiSkin.GetStyle("textfield"));
            if (name != UserName && FormValidator.IsValidName(name))
                UserName = name;
            GUILayout.FlexibleSpace();
            GUILayout.EndVertical();
            confirmButton.Draw();
            GUILayout.EndHorizontal();
        }

        private void OnConfirmClick(ButtonRenderer button)
        {
            if (FormValidator.IsValidName(UserName))
            {
                Confirm(UserName);
            }
        }
    }
}
