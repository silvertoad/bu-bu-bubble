using System;
using System.Collections;
using Assets.Scripts;
using Assets.Scripts.GUI.Components;
using Assets.Scripts.network;
using Assets.Scripts.network.data;
using BubblesGame.GUI.Components;
using UnityEngine;
using BubblesGame.Utils;

namespace BubblesGame.GUI.Screens
{
    /// <summary>
    /// Экран сетевого меню
    /// </summary>
    class NetworkMenuScreen : IScreen
    {
        ///<summary>Событие завершения экрана</summary>
        public event Action<ScreenExitEvent> Exit;
        
        private GameObject _go;
        
        private NetworkMenuBehaviour _screen;

        /// <summary>
        /// Инициализатор экрана сетевого меню
        /// </summary>
        /// <param name="param">Параметры инициализации</param>
        public void Init(Hashtable param)
        {
            _go = new GameObject();
            _screen = _go.AddComponent<NetworkMenuBehaviour>();
            _screen.Exit += screen_Exit;
        }

        private void screen_Exit(ScreenExitEvent obj)
        {
            _screen.Exit -= screen_Exit;
            GameObject.DestroyObject(_go);
            Exit(obj);
        }
    }

    /// <summary>
    /// Рендерер экрана сетевого меню
    /// </summary>
    internal sealed class NetworkMenuBehaviour : MonoBehaviour
    {
        ///<summary>Событие выхода из сетевого меню</summary>
        public event Action<ScreenExitEvent> Exit;

        private static Vector2 NormalConnectBtnSize = new Vector2(165, 80);
        private static Vector2 MaxConnectBtnSize = new Vector2(170, 100);
        private static Vector2 NormalBackBtnSize = new Vector2(115, 80);
        private static Vector2 MaxBackBtnSize = new Vector2(120, 100);

        private static string host = "127.0.0.1";
        private static string port = "8888";

        private ButtonRenderer connectButton;
        private ButtonRenderer backButton;

        private GUISkin guiSkin;

        void Start()
        {
            guiSkin = (GUISkin)AssetManager.GetAsset("MainSkin");

            connectButton = new ButtonRenderer("Connect", new Rect(40, 180, 170, 100), NormalConnectBtnSize)
                .AddTween(new Tween("Size", TweenType.Lerp, NormalConnectBtnSize, MaxConnectBtnSize, 80))
                .AddTween(new Tween("Size", TweenType.Lerp, MaxConnectBtnSize, NormalConnectBtnSize, 100, 90));
            connectButton.OnClick += OnConnectClick;

            backButton = new ButtonRenderer("Back", new Rect(210, 180, 120, 100), NormalBackBtnSize)
                .AddTween(new Tween("Size", TweenType.Lerp, NormalBackBtnSize, MaxBackBtnSize, 100, 80))
                .AddTween(new Tween("Size", TweenType.Lerp, MaxBackBtnSize, NormalBackBtnSize, 150, 190));
            backButton.OnClick += OnBackClick;
        }

        void OnGUI()
        {
            UnityEngine.GUI.skin = guiSkin;

            GUILayout.Window(0, WindowLayout.GetCenteredRect(370, 330), UpdateDialog, "Connect To");
        }

        // Отрисовка окна сетевого меню
        private void UpdateDialog(int windowId)
        {
            GUILayout.BeginHorizontal();
                GUILayout.Label("Host:");
                var hostStr = GUILayout.TextField(host, guiSkin.GetStyle("textfield"));
                if (host != hostStr && FormValidator.IPAddressSymbols(hostStr))
                    host = hostStr;
            GUILayout.EndHorizontal();

            GUILayout.Space(5);

            GUILayout.BeginHorizontal();
                GUILayout.Label("Port:");
                var portStr = GUILayout.TextField(port, guiSkin.GetStyle("textfield"));
                if (port != portStr && FormValidator.IsDigits(port))
                    port = portStr;
            GUILayout.EndHorizontal();

            GUILayout.Space(15);

            GUILayout.BeginHorizontal();
            connectButton.Draw();
            backButton.Draw();
            GUILayout.EndHorizontal();
        }

        private void OnConnectClick(ButtonRenderer button)
        {
            if (FormValidator.IPAddressRestricted(host) && FormValidator.PortRestricted(port))
            {
                var data = new Hashtable {{"host", host}, {"port", short.Parse(port)}};
                Exit(new ScreenExitEvent(ScreenType.OutboxConnection, data));
            }
        }

        private void OnBackClick(ButtonRenderer button)
        {
            Exit(new ScreenExitEvent(ScreenType.MainMenu));
        }
    }
}
