﻿using System;
using Assets.Scripts.GUI.Components;
using BubblesGame.Utils;
using UnityEngine;

namespace BubblesGame.GUI.Components
{
    /// <summary>
    /// Унифицированное окно с одной кнопкой
    /// </summary>
    public class InfoWindowRenderer : IDisposable
    {
        public event Action Confirm;

        private static Vector2 NormalCancelBtnSize = new Vector2(165, 80);
        private static Vector2 MaxCancelBtnSize = new Vector2(170, 100);

        private Vector2 windowSize;

        private CenteredLabelRenderer infoText;
        private ButtonRenderer button;

        private GUISkin guiSkin;

        public InfoWindowRenderer(string header, string text, int windowId = 0)
            : this(UnityEngine.GUI.skin, header, text, null)
        {
        }

        public InfoWindowRenderer(GUISkin skin, string header, string text, int windowId = 0)
            : this(skin, header, text, null, windowId)
        {
        }

        /// <summary>
        /// Создать окно с конопкой по расширенным парамертрам
        /// </summary>
        /// <param name="skin">Скин</param>
        /// <param name="header">Заголовок</param>
        /// <param name="text">Текст в окне</param>
        /// <param name="buttonLabel">Текст на кнопке</param>
        /// <param name="windowId">id рендерера</param>
        public InfoWindowRenderer(GUISkin skin, string header, string text, string buttonLabel, int windowId = 0)
        {
            guiSkin = skin;

            Id = windowId;

            windowSize = new Vector2(450, 260);

            infoText = new CenteredLabelRenderer(text, new Vector2(windowSize.x - 80, 30), guiSkin);

            button = new ButtonRenderer(buttonLabel, new Rect(220, 135, 170, 100), NormalCancelBtnSize)
                .AddTween(new Tween("Size", TweenType.Lerp, NormalCancelBtnSize, MaxCancelBtnSize, 80))
                .AddTween(new Tween("Size", TweenType.Lerp, MaxCancelBtnSize, NormalCancelBtnSize, 100, 90));
            button.OnClick += OnButtonClick;
            
            Header = header == null ? "Info" : header;
            Text = text == null ? "<null description>" : text;
            ButtonLabel = buttonLabel == null ? "OK" : buttonLabel;
        }

        public int Id { get; set; }

        public string Text
        {
            get { return infoText.Text; }
            set { infoText.Text = value; }
        }

        public string Header { get; set; }
        
        public string ButtonLabel
        {
            get { return button.Label; }
            set { button.Label = value; }
        }

        public void Draw()
        {
            GUILayout.Window(0, WindowLayout.GetCenteredRect(windowSize), Update, Header);
        }

        private void Update(int windowId)
        {
            infoText.Draw();
            button.Draw();
        }

        private void OnButtonClick(ButtonRenderer button)
        {
            Confirm();
        }

        public void Dispose()
        {
            button.Dispose();
        }
    }
}
