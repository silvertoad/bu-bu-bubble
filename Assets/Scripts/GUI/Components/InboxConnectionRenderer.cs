﻿using UnityEngine;
using Assets.Scripts.network;

namespace BubblesGame.GUI.Components
{
    public class InboxConnectionRenderer : InboxMessageRenderer
    {
        /// <summary>
        /// Принять или отклонить игру
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <param name="skin">Скин</param>
        public InboxConnectionRenderer(InboxMessage message, GUISkin skin)
            : base(message, skin)
        {
            Size = new Vector2(300, 90);
        }

        public string Text
        {
            get { return "Play with " + Message.Sender.Name + "?"; }
        }

        /// <summary>
        /// Отрисовать кнопки
        /// </summary>
        public override void Draw()
        {
            base.Draw();

            GUILayout.Label(Text, Skin.GetStyle("CenteredLabel"), GUILayout.Height(30));
            GUILayout.BeginHorizontal(GUILayout.Height(50));
            if (GUILayout.Button("Yes", GUILayout.Width(80)))
            {
                Accept();
            }
            else if (GUILayout.Button("No", GUILayout.Width(70)))
            {
                Skip();
            }
            GUILayout.EndHorizontal();
        }
    }
}
