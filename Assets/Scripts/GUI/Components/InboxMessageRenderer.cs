﻿using Assets.Scripts.network;
using UnityEngine;

namespace BubblesGame.GUI.Components
{
    public class InboxMessageRenderer
    {
        /// <summary>
        /// Базовый класс для отрисовки сообщения inbox
        /// </summary>
        /// <param name="message">Пришедшее сообщение</param>
        /// <param name="skin">Скин</param>
        public InboxMessageRenderer(InboxMessage message, GUISkin skin = null)
        {
            Message = message;
            Skin = skin;
        }

        public InboxMessage Message { get; private set; }

        // Отрисовать
        public virtual void Draw()
        {
            if (!Message.IsReaded)
                Message.Open();
        }

        // Отклонить
        public void Skip()
        {
            Message.Skip();
        }

        // Принять
        public void Accept()
        {
            Message.Accept();
        }

        public Vector2 Size { get; set; }
        public GUISkin Skin { get; set; }
    }
}
