﻿using System;
using UnityEngine;
using BubblesGame.Utils;

namespace BubblesGame.GUI.Components
{
    /// <summary>
    /// Кастомизированная кнопка для inbox
    /// </summary>
    class InboxButtonRenderer
    {
        // Событие нажатия
        public event Action Click = delegate { };

        private int numMessages;

        private static Vector2 NormalSize = new Vector2(70, 70);
        private static Vector2 MaxSize = new Vector2(80, 80);

        private Tweener tweener;
        public Vector2 Size { get; private set; }
        private GUISkin skin;

        /// <summary>
        /// Проинитить кнопку своим скином
        /// </summary>
        /// <param name="guiSkin">скин</param>
        public InboxButtonRenderer(GUISkin guiSkin)
        {
            skin = guiSkin;
            Size = NormalSize;
        }

        /// <summary>
        /// Геттер-сеттер числа сообщений inbox
        /// </summary>
        public int NumMessages
        {
            get { return numMessages; }
            set
            {

                if (numMessages != value)
                {
                    numMessages = value;
                    Size = MaxSize;
                    StopTweens();
                    tweener = new Tweener(this)
                        .Add(new Tween("Size", TweenType.Lerp, MaxSize, NormalSize, 200))
                        .Start();
                }
            }
        }

        /// <summary>
        /// Отрисоввать кнопку с цифрой сообщений
        /// </summary>
        public void Draw()
        {
            if (tweener != null)
                tweener.Update();

            if (GUILayout.Button(numMessages.ToString(), skin.GetStyle("Inbox"), GUILayout.Width(Size.x), GUILayout.Height(Size.y)))
            {
                Click();
            }
        }

        private void StopTweens()
        {
            Tweener tw = tweener;
            if (tw != null)
            {
                tweener = null;
                tw.Dispose();
            }
        }
    }
}
