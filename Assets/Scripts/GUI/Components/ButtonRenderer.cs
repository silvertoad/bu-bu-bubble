﻿using System;
using BubblesGame.Utils;
using UnityEngine;

namespace Assets.Scripts.GUI.Components
{
    class ButtonRenderer : IDisposable
    {
        // Событие нажатия
        public event Action<ButtonRenderer> OnClick;

        /// <summary>
        /// Отрисовать кнопку вписанную в контейнер
        /// </summary>
        /// <param name="label">Текст</param>
        /// <param name="maxBounds">Контейнер</param>
        /// <param name="size">Размер кнопки</param>
        /// <param name="relative">Использовать ли смещение</param>
        public ButtonRenderer(string label, Rect maxBounds, Vector2 size, bool relative = false)
        {
            Bounds = maxBounds;
            Size = size;
            Label = label;
            Relative = relative;
        }

        public Rect Bounds { get; private set; }
        public Vector2 Size { get; set; }
        public string Label { get; set; }
        public bool Relative { get; set; }

        private Tweener tweener;

        /// <summary>
        /// Запуск твинера с анимацией появления кнопки
        /// </summary>
        /// <param name="tween"></param>
        /// <returns></returns>
        public ButtonRenderer AddTween(Tween tween)
        {
            if (tweener == null)
            {
                tweener = new Tweener(this);
                tweener.Complete += OnCompleteTweens;
            }
            tweener.Add(tween);
            if (!tweener.IsRunning)
                tweener.Start();
            return this;
        }

        private void OnCompleteTweens(Tweener tw)
        {
            tweener = null;
        }

        /// <summary>
        /// Отрисовка кнопки
        /// </summary>
        public void Draw()
        {
            if (tweener != null)
            {
                tweener.Update();
            }

            if (Relative)
            {
                GUILayout.BeginVertical(GUILayout.Width(Bounds.width), GUILayout.Height(Bounds.height));
            }
            else
            {
                GUILayout.BeginArea(Bounds);
            }
            GUILayout.FlexibleSpace();
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button(Label, GUILayout.Width(Size.x), GUILayout.Height(Size.y)))
            {
                OnClick(this);
            }
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            GUILayout.FlexibleSpace();
            if (Relative)
            {
                GUILayout.EndVertical();
            }
            else
            {
                GUILayout.EndArea();
            }
        }

        public void Dispose()
        {
            if (tweener != null)
            {
                tweener.Dispose();
                tweener = null;
            }
        }
    }
}
