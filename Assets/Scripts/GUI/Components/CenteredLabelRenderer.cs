﻿using UnityEngine;

namespace BubblesGame.GUI.Components
{
    class CenteredLabelRenderer
    {
        /// <summary>
        /// Вписать лэйбл по центру в заданный размер size
        /// </summary>
        /// <param name="text">Текст</param>
        /// <param name="size">Размер</param>
        /// <param name="skin">Шкура</param>
        public CenteredLabelRenderer(string text, Vector2 size, GUISkin skin)
        {
            Text = text;
            Size = size;
            Skin = skin;
        }

        public Vector2 Size { get; set; }
        public string Text { get; set; }
        public GUISkin Skin { get; set; }

        /// <summary>
        /// Отрисовать лэйбл
        /// </summary>
        public void Draw()
        {
            GUILayout.BeginHorizontal(GUILayout.Width(Size.x), GUILayout.Height(Size.y));
            GUILayout.FlexibleSpace();
            GUILayout.BeginVertical();
            GUILayout.FlexibleSpace();
            GUILayout.Label(Text, Skin.GetStyle("CenteredLabel"));
            GUILayout.FlexibleSpace();
            GUILayout.EndVertical();
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
        }
    }
}
