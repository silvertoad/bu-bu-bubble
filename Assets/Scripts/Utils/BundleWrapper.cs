﻿using System;
using System.Collections;
using UnityEngine;

namespace BubblesGame.Utils
{
    class BundleWrapper
    {
        public event Action GameDataLoaded = delegate { };
        public static AssetBundle Bundle;
        
        /// <summary>
        /// Загрузка основного пакета с данными
        /// После окончания шлется событие GameDataLoaded
        /// </summary>
        /// <returns></returns>
        public IEnumerator ProcessGamePack()
        {
            var www = WWW.LoadFromCacheOrDownload("file://" + Application.dataPath + "/Resources/Bundles/GamePack.unity3d", 11);
            yield return www;

            if (www.error != null)
                throw new Exception("Asset Bundle download error:" + www.error);

            Bundle = www.assetBundle;

            GameDataLoaded();
        }
    }
}
