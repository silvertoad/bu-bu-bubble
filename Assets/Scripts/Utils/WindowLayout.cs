﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace BubblesGame.Utils
{
    class WindowLayout
    {
        public static Rect GetCenteredRect(float width, float height, float boundWidth, float boundHeight)
        {
            float x = (boundWidth - width) / 2;
            float y = (boundHeight - height) / 2;
            return new Rect(x, y, width, height);
        }

        public static Rect GetCenteredRect(int width, int height, int boundWidth, int boundHeight)
        {
            int x = (boundWidth - width) >> 1;
            int y = (boundHeight - height) >> 1;
            return new Rect(x, y, width, height);
        }

        public static Rect GetCenteredRect(int width, int height)
        {
            return GetCenteredRect(width, height, Screen.width, Screen.height);
        }

        public static Rect GetCenteredRect(Vector2 size)
        {
            return GetCenteredRect(size.x, size.y, Screen.width, Screen.height);
        }

        public static Rect GetCenteredRect(Vector2 size, Vector2 bounds)
        {
            return GetCenteredRect(size.x, size.y, bounds.x, bounds.y);
        }
    }
}
