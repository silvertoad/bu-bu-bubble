﻿using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

namespace BubblesGame.Utils
{
    internal class SoundManager
    {
        public static String MAIN_THEME = "Happy Tree Friends - Theme";
        public static String BALLOON_POPS = "Balloon Pops Sound Effect";

        public event Action SoundDataLoaded = delegate { };

        private static AssetBundle _bundle;
        private static AudioClip _balloonPops;

        public SoundManager()
        {
        }

        /// <summary>
        /// Подгружаем bundle со звуковыми ассетами
        /// </summary>
        /// <returns></returns>
        public IEnumerator PrepareBundle()
        {
            var www = WWW.LoadFromCacheOrDownload("file://" + Application.dataPath + "/Resources/Bundles/SoundPack.unity3d", 1);
            yield return www;

            if (www.error != null)
                throw new Exception("Sound Bundle download error:" + www.error);

            _bundle = www.assetBundle;

            // Предподготавливаем аудио клип
            _balloonPops = _bundle.Load(BALLOON_POPS, typeof (AudioClip)) as AudioClip;

            SoundDataLoaded();
        }

        /// <summary>
        /// Проиграть звук для определенного источника
        /// </summary>
        /// <param name="obj">Источник звука</param>
        /// <param name="name">Имя звукового ассета</param>
        /// <param name="loop">Зацикливать?</param>
        /// <returns>Длинаа звукового клипа</returns>
        public static float PlaySound(AudioSource obj, String name, Boolean loop)
        {
            if (_bundle == null)
                throw new Exception("Sound Bundle is null");

            var sound = _bundle.Load(name, typeof (AudioClip)) as AudioClip;
            if (sound == null)
                throw new Exception("Audio Source not found " + name);

            obj.audio.loop = loop;
            obj.audio.clip = sound;
            obj.audio.Play();

            return sound.length;
        }

        /// <summary>
        /// Проиграть звук лопанья шарика, аудиоклип для него предподготовлен для улучшения производительности
        /// </summary>
        /// <param name="obj">Источник звука</param>
        /// <returns>Длинаа звукового клипа</returns>
        public static float PlayBallonPops(AudioSource obj)
        {
            obj.audio.clip = _balloonPops;
            obj.audio.Play();

            return _balloonPops.length;
        }
    }
}
