﻿using System;

namespace BubblesGame.Utils
{
    public static class RandomExtentions
    {
        public static float Range(this Random random, float min, float max)
        {
            if (min == max)
                return min;
            return (float) (min + (max - min)*random.NextDouble());
        }
    }
}
