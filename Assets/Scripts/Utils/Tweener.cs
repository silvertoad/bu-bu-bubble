﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

namespace BubblesGame.Utils
{
    public class Tweener : IDisposable
    {
        private List<Tween> tweens;

        private object target;

        public Action<Tween> TweenComplete = delegate { };
        public Action<Tweener> Complete = delegate { };

        private Stopwatch timer;

        public Tweener(object targetObject)
        {
            target = targetObject;
            tweens = new List<Tween>();
            timer = new Stopwatch();
        }

        internal object Target { get { return target; } }

        internal List<Tween> Tweens { get { return tweens; } }

        public bool IsRunning { get { return timer != null && timer.IsRunning && !Completed; } }
        public bool Completed { get; private set; }

        public Tweener Add(Tween tween)
        {
            tweens.Add(tween);
            return this;
        }

        public Tweener Start()
        {
            timer.Start();
            return this;
        }

        public void Update()
        {
            if (!IsRunning)
                return;

            long elapsed = timer.ElapsedMilliseconds;

            int len = tweens.Count;
            while(len-- != 0)
            {
                Tween tween = tweens[len];
                TryUpdateTween(tween, elapsed);
            }

            if (tweens.Count == 0)
            {
                Stop();
            }
        }

        public void Stop()
        {
            Complete(this);
            Dispose();
        }

        public void Dispose()
        {
            if (timer == null) return;
            
            timer.Stop();
            timer = null;
            tweens = null;
            target = null;
        }

        private void CompleteTween(Tween tween)
        {
            tweens.Remove(tween);
            TweenComplete(tween);
        }

        private void TryUpdateTween(Tween tween, long elapsedTime)
        {
            if (elapsedTime >= tween.delay)
            {
                long tweenTime = elapsedTime - tween.delay;

                if (tweenTime <= tween.duration)
                {
                    UpdateTween(tween, tweenTime);
                }
                else
                {
                    CompleteTween(tween);
                }
            }
        }

        private void UpdateTween(Tween tween, long tweenTime)
        {
            switch (tween.type)
            {
                case TweenType.Lerp:
                    Lerp(tween, tweenTime);
                    break;
                default:
                    throw new Exception("Unknown TweenType running " + tween.type.ToString());
            }
        }

        private void Lerp(Tween tween, long lostTime)
        {
            float frac = (float)lostTime / (float)tween.duration;
            
            var currentValue = GetParamValue(tween.param);

            if (currentValue is Vector3)
            {
                SetParamValue(tween.param, Vector3.Lerp((Vector3)tween.from, (Vector3)tween.to, frac));
            }
            else if (currentValue is Vector2)
            {
                SetParamValue(tween.param, Vector2.Lerp((Vector2)tween.from, (Vector2)tween.to, frac));
            }
            else if (currentValue is Vector3)
            {
                SetParamValue(tween.param, Vector3.Lerp((Vector3)tween.from, (Vector3)tween.to, frac));
            }
            else if (currentValue is float)
            {
                SetParamValue(tween.param, Mathf.Lerp((float)tween.from, (float)tween.to, frac));
            }
            else if (currentValue is int)
            {
                SetParamValue(tween.param, (int)Mathf.Lerp((float)tween.from, (float)tween.to, frac));
            }
            else if (currentValue is Color)
            {
                Color from = (Color)tween.from;
                Color to = (Color)tween.to;
                float r = Mathf.Lerp(from.r, to.r, frac);
                float g = Mathf.Lerp(from.g, to.g, frac);
                float b = Mathf.Lerp(from.b, to.b, frac);
                float a = Mathf.Lerp(from.a, to.a, frac);
                SetParamValue(tween.param, new Color(r, g, b, a));
            }
            else
            {
                throw new Exception("Unknown field type to operate Lerp: " + currentValue.GetType());
            }
        }

        public object GetParamValue(string param)
        {
            var field = target.GetType().GetField(param);
            if (field != null)
            {
                return field.GetValue(target);
            }
            var property = target.GetType().GetProperty(param);
            if (property != null)
            {
                return property.GetValue(target, null);
            }
            throw new Exception("Unknown property " + param + " for target: " + target.ToString());
        }

        public void SetParamValue(string param, object value)
        {
            var field = target.GetType().GetField(param);
            if (field != null)
            {
                field.SetValue(target, value);
                return;
            }
            var property = target.GetType().GetProperty(param);
            if (property != null)
            {
                property.SetValue(target, value, null);
                return;
            }
            throw new Exception("Unknown property " + param + " for target: " + target.ToString());
        }
    }

    public struct Tween
    {
        public string param;
        public long delay;
        public long duration;
        public object from;
        public object to;
        public TweenType type;

        public Tween(string param, TweenType type, object from, object to, long duration = 1000, long delay = 0)
        {
            this.param = param;
            this.delay = delay;
            this.duration = duration;
            this.from = from;
            this.to = to;
            this.type = type;
        }
    }

    public enum TweenType
    {
        Lerp
    }
}
