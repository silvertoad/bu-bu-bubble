﻿using System.Collections.Generic;

namespace BubblesGame.Utils
{
    class AssetManager
    {
        static Dictionary<string, UnityEngine.Object> _assets = new Dictionary<string, UnityEngine.Object>();

        /// <summary>
        /// Подгрузить объект из игрового пакета bundle   ``
        /// </summary>
        /// <param name="url">Идентификатов</param>
        /// <returns>Искомый объект</returns>
        public static UnityEngine.Object GetAsset(string url)
        {
            if (!_assets.ContainsKey(url))
            {
                _assets[url] = BundleWrapper.Bundle.Load(url);
            }
            return _assets[url];
        }
    }
}
