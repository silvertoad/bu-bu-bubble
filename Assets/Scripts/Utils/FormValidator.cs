﻿using System;
using System.Text.RegularExpressions;

namespace BubblesGame.Utils
{
    class FormValidator
    {
        public static bool PortRestricted(string text)
        {
            return text.Length < 6 && Regex.IsMatch(text, @"^\d{1,5}$") && decimal.Parse(text) < (decimal)short.MaxValue;
        }

        public static bool IPAddressRestricted(string text)
        {
            return text.Length < 16 && Regex.IsMatch(text, @"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$");
        }

        public static bool IPAddressSymbols(string text)
        {
            return text.Length < 16 && Regex.IsMatch(text, @"^[.0-9]*?$");
        }

        public static bool IsDigits(string text)
        {
            return text.Length < 20 && Regex.IsMatch(text, @"^\d*?$");
        }

        public static bool IsValidName(string text)
        {
            return text.Length < 15 && Regex.IsMatch(text, @"^(\w*?\s?)+$");
        }
    }
}
