﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Debug = UnityEngine.Debug;
using Random = System.Random;

namespace BubblesGame
{
    public class ResourceManager
    {
        public event Action SetCreated = delegate { };

        public const Int16 TEXTURE_32 = 32;
        public const Int16 TEXTURE_64 = 64;
        public const Int16 TEXTURE_128 = 128;
        public const Int16 TEXTURE_256 = 256;

        private AssetBundle _bundle;

        private Boolean _isLoadign;

        private readonly List<String> _happyNames;

        private Dictionary<Int16, Dictionary<Color, Texture2D>> _texturesSet;

        private Dictionary<Int16, Dictionary<Color, Texture2D>> _newTexturesSet;
        private Dictionary<Int16, List<Color>> _newSetData;

        private Dictionary<string, UnityEngine.Object> _assetCache = new Dictionary<string, UnityEngine.Object>();

        public ResourceManager(AssetBundle bundle)
        {
            _bundle = bundle;
            _happyNames = new List<string>
            {
                "Cub",
                "Cuddles",
                "Flippy",
                "Giggles",
                "Lifty",
                "Mime",
                "Nutty",
                "Petunie",
                "Splendid",
                "TheMole",
                "Toothy"
            };
        }

        /// <summary>
        /// Принимаем команду на создание и подготовку нового сета текстур
        /// </summary>
        /// <param name="newSetData"></param>
        public void CreateSet(Dictionary<Int16, List<Color>> newSetData)
        {
            if (_isLoadign)
                throw new Exception("Previous set is loading");
            
            _isLoadign = true;
            _newSetData = newSetData;

        }

        /// <summary>
        /// Переключить преподготовненные данные по текстурам
        /// Предыдущий сет дестроится
        /// </summary>
        public void SwitchSet()
        {
            if (_isLoadign)
                throw new Exception("New level data don't inited yet");

            if (_newTexturesSet.Count == 0)
                throw new Exception("No one item in the new set");

            if (_texturesSet != null)
                DestroyCurrentSet();

            _texturesSet = new Dictionary<short, Dictionary<Color, Texture2D>>(_newTexturesSet);
            _newTexturesSet.Clear();
        }

        /// <summary>
        /// Разрушить текущий набор текстур
        /// </summary>
        private void DestroyCurrentSet()
        {
            foreach (var item in _texturesSet)
            {
                foreach (var colorItem in item.Value)
                {
                    UnityEngine.Object.Destroy(colorItem.Value);
                }
            }
            _texturesSet.Clear();
        }

        /// <summary>
        /// Получить текстуру по размеру и цвету объекта
        /// </summary>
        /// <param name="size">Цвет объекта</param>
        /// <param name="color">Размер объекта</param>
        /// <returns></returns>
        public Texture2D GetTexture(Int16 size, Color color)
        {
            return _texturesSet[size][color];
        }

        /// <summary>
        /// Получить текстуру по имени объета
        /// Загружаем данные из подготовленного bunlde
        /// </summary>
        /// <param name="name">Имя объекта</param>
        /// <returns></returns>
        public Texture2D GetTexture(String name)
        {
            if (!_assetCache.ContainsKey(name))
            {
                _assetCache[name] = _bundle.Load(name);
            }

            if (_assetCache[name] == null)
                throw new Exception("Texture " + name + " not found in bundle");

            return _assetCache[name] as Texture2D;
        }

        /// <summary>
        /// Получить игровой объект 
        /// </summary>
        /// <param name="name">Имя объекта</param>
        /// <returns></returns>
        public GameObject GetGameObject(String name)
        {
            if (!_assetCache.ContainsKey(name))
            {
                _assetCache[name] = _bundle.Load(name);
            }
            if (_assetCache[name] == null)
                throw new Exception("GameObject " + name + " not found in bundle");

            return _assetCache[name] as GameObject;
        }

        /// <summary>
        /// Обновление, инициализация механизма размазывания преподготовки ассетов
        /// </summary>
        public void Update()
        {
            if (!_isLoadign)
                return;

            if (_newTexturesSet == null)
                _newTexturesSet = new Dictionary<short, Dictionary<Color, Texture2D>>();

            var size = _newSetData.Keys.ElementAt(0);
            var colors = _newSetData[size];
            var color = colors[0];

            if (!_newTexturesSet.ContainsKey(size))
                _newTexturesSet[size] = new Dictionary<Color, Texture2D>();

            // Профайл производительности
            //var sw = Stopwatch.StartNew();
            _newTexturesSet[size][color] = CombineTextures(size, CreateGradient(size, color), GetHappyTexture(size));
            //Debug.Log("GEN TIME: " + sw.Elapsed);

            colors.Remove(color);
            
            if (colors.Count == 0)
                _newSetData.Remove(size);               

            if (_newSetData.Count == 0)
            {
                _isLoadign = false;
                SetCreated();
            }
        }

        /// <summary>
        /// Собрать данные для градиентной составляющей ассета
        /// Градиент уходит в белый цвет
        /// </summary>
        /// <param name="size">Высота градиента</param>
        /// <param name="color">Цвет градиента</param>
        /// <returns>Массив цветов градиента</returns>
        private Color[] CreateGradient(Int16 size, Color color)
        {
            var result = new Color[size];

            for (var i = 0; i < size; i++)
            {
                result[i] = Linear((float) i / size, color);
            }

            return result;
        }

        /// <summary>
        /// Просчитать цвет для позиции градиента
        /// </summary>
        /// <param name="val">Текущая позиция</param>
        /// <param name="targetColor">Стартовый цвет</param>
        /// <returns>Результирующий цвет градиента</returns>
        private Color Linear(float val, Color targetColor)
        {
            var r = targetColor.r * val + Color.white.r * (1 - val);
            var g = targetColor.g * val + Color.white.g * (1 - val);
            var b = targetColor.b * val + Color.white.b * (1 - val);
            return new Color(r, g, b);
        }

        /// <summary>
        /// Погрузить текстуру из bundle
        /// Выбирется рандомная мордаха и загружается в текстуру
        /// </summary>
        /// <param name="size">Требуемый размер</param>
        /// <returns>Текстура с мордахой</returns>
        private Texture2D GetHappyTexture(Int16 size)
        {
            var rnd = new System.Random();
            var name = _happyNames[rnd.Next(_happyNames.Count)];
            name += "_" + size + "x" + size + "x32";

            return _bundle.Load(name) as Texture2D;
        }

        /// <summary>
        /// Слепить ассет мордахи и градиентную составляющую в одну текстуру
        /// Запекание происходит кодом, по оптимизированному алгоритму
        /// </summary>
        /// <param name="size">Исходный размер</param>
        /// <param name="gradient">Массил цветов для градиента</param>
        /// <param name="happyTexture">Текстура с мордахой</param>
        /// <returns>Результирующая текструра с мордахой на градиентном фоне</returns>
        private Texture2D CombineTextures(Int16 size, Color[] gradient, Texture2D happyTexture)
        {
            var result = new Texture2D(size, size, TextureFormat.RGB24, false);
            var pixels = happyTexture.GetPixels();

            var len = size * size;
            var colors = new Color[len];

            for (var i = 0; i < len; i++)
            {
                var p1 = gradient[i / size];
                var p2 = pixels[i];

                colors[i]  = new Color(
                    p2.r * p2.a + p1.r * (1 - p2.a),
                    p2.g * p2.a + p1.g * (1 - p2.a),
                    p2.b * p2.a + p1.b * (1 - p2.a),
                    p2.a * p2.a + p1.a * (1 - p2.a));
            }

            result.SetPixels(colors);
            result.Apply();

            // Запись в файл для просмотра результата
            //var rnd = new Random();
            //var bytes = result.EncodeToPNG();
            //File.WriteAllBytes("Assets/Resources/Textures/happytreefriends/generated/" + rnd.Next().ToString() + ".png", bytes);

            return result;
        }
    }
}
