﻿using System;
using System.Diagnostics;
using BubblesGame.Models.Game.LevelGeneration;
using BubblesGame.Models.Interfaces;
using BubblesGame.Models.Player;

namespace BubblesGame.Models
{
    /// <summary>
    /// Игровой контроллер для одиночной игры
    /// </summary>
    class SinglePlayerLevelController : ILevelController
    {
        /// <summary>
        /// Игра закончена
        /// </summary>
        public event Action GameOver = delegate {};
        
        /// <summary>
        /// Игрок завершил уровень (один из многих)
        /// </summary>
        public event Action Complete = delegate {};

        protected GameLevel Level;
        protected PlayerModel Player;

        public SinglePlayerLevelController(PlayerModel player)
        {            
            Player = player;    
        }

        /// <summary>
        /// Установка игрового уровня
        /// </summary>
        /// <param name="level"></param>
        public void SetLevel(GameLevel level)
        {
            if (Level != null)
            {
                Level.ObjectFeltDown -= OnObjectFeltDown;
                Level.LevelEnded -= OnLevelEnded;    
            }
            Level = level;
            Level.ObjectFeltDown += OnObjectFeltDown;
            Level.LevelEnded += OnLevelEnded;
        }

        /// <summary>
        /// Обработка клика по объекту
        /// </summary>
        /// <param name="id"></param>
        public virtual void OnMouseDown(int id)
        {
            var ball = Level.GetBallModel(id);
            Level.Shoot(ball);
            Player.AddScore(ball.Score);
        }
        
        /// <summary>
        /// Сдвиг игрового времени
        /// </summary>
        /// <param name="deltaTime"></param>
        public virtual void Rewind(float deltaTime)
        {
            if (Level != null)
            {
                Level.Rewind(deltaTime);
            }
        }

        protected virtual void End()
        {
            Clear();
            GameOver();
        }

        private void Clear()
        {
            if (Level != null)
            {
                Level.ObjectFeltDown -= OnObjectFeltDown;
                Level.LevelEnded -= OnLevelEnded;
                Level = null; 
            } 
        }

        private void OnLevelEnded()
        {
            Clear();
            Complete();
        }

        private void OnObjectFeltDown(IGameObjectModel obj)
        {
            Player.AddHP(-1);
            if (Player.HP <= 0)
            {
                End();
            }
        }
    }
}
