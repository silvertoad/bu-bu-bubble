﻿namespace BubblesGame.Models.Interfaces
{
    /// <summary>
    /// Интерфейс контроллера игрового процесса
    /// </summary>
    public interface ILevelController : IRewindable
    {
        /// <summary>
        /// Обработчик нажатия на игровой объект
        /// </summary>
        /// <param name="id">идентификатор объекта</param>
        void OnMouseDown(int id);
    }
}