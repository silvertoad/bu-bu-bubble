﻿using System;

namespace BubblesGame.Models.Interfaces
{
    public interface IGameLevel
    {
        /// <summary>
        /// Создан новый игровой объект
        /// </summary>
        event Action<IGameObjectModel> NewObjectCreated;

        /// <summary>
        /// Объект упал за нижнюю планку и исчез
        /// </summary>
        event Action<IGameObjectModel> ObjectFeltDown;

        /// <summary>
        /// Объект удалился
        /// </summary>
        event Action<IGameObjectModel> ObjectRemoved;

        /// <summary>
        /// Уровень завершился
        /// </summary>
        event Action LevelEnded;
    }
}