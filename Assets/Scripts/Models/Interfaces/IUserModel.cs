﻿using System;

namespace Assets.Scripts.network.data
{
    public interface IUserModel
    {
        /// <summary>
        /// Интерфейс для игрока сетевой игры, реализует его имя и seed для передачи по сокету
        /// </summary>
        string Name { get; set; }
        Int32 Seed { get; }
        UserInfo ToSerializable(Boolean accepted);
    }
}