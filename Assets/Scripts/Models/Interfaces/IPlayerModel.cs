﻿using System;
using Assets.Scripts.network.data;

namespace BubblesGame.Models.Interfaces
{
    /// <summary>
    /// Интерфейс для модели игрока
    /// </summary>
    public interface IPlayerModel : IDisposable
    {
        /// <summary>
        /// Событие изменения количества очков
        /// </summary>
        event Action ScoreChanged;

        /// <summary>
        /// Событие изменения HP
        /// </summary>
        event Action HPChanged;

        /// <summary>
        /// Данные пользователя
        /// </summary>
        IUserModel User { get; }
        
        /// <summary>
        /// Очки игрока
        /// </summary>
        int Score { get; }

        /// <summary>
        /// Количество жизней игрока
        /// </summary>
        int HP { get; }
    }
}