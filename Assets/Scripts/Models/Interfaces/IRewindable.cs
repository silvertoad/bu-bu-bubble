﻿namespace BubblesGame.Models.Interfaces
{
    /// <summary>
    /// Интерфейс для сущностей, в которых можно "перемотать время". 
    /// Например, когда мы применяем сдвиг времени к модели игрового уровня, 
    /// то он в ответ диспатчит события, которые говорят о том, что 
    /// произошло за это время.
    /// </summary>
    public interface IRewindable
    {
        void Rewind(float deltaTime);
    }
}