﻿using System;
using BubblesGame.Models.Game.LevelGeneration;
using UnityEngine;

namespace BubblesGame.Models.Interfaces
{
    /// <summary>
    /// Модель игрового объекта
    /// </summary>
    public interface IGameObjectModel
    {
        /// <summary>
        /// Объект удален
        /// </summary>
        event Action Removed;

        /// <summary>
        /// Объект переместился
        /// </summary>
        event Action Moved;

        /// <summary>
        /// Координата Х
        /// </summary>
        float X { get; }

        /// <summary>
        /// Координата Y
        /// </summary>
        float Y { get; }

        /// <summary>
        /// Координата Z
        /// </summary>
        float Z { get; }

        /// <summary>
        /// Уникальный id объекта
        /// </summary>
        int Id { get; }

        /// <summary>
        /// Размер объекта
        /// </summary>
        float Size { get; }

        /// <summary>
        /// Размер для текстуры объекта
        /// </summary>
        Int16 TextureSize { get; }

        /// <summary>
        /// Цвет объекта
        /// </summary>
        Color Clr { get; }

        /// <summary>
        /// Количество очков за уничтожение
        /// </summary>
        int Score { get; }
    }
}