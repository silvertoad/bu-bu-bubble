﻿using UnityEngine;

namespace BubblesGame.Models.Interfaces
{
    /// <summary>
    /// Интерфейс для фабрики отображений игровых объектов
    /// </summary>
    public interface IGameObjectViewFactory
    {
        /// <summary>
        /// Создать GameObject отображения шара
        /// </summary>
        /// <returns></returns>
        GameObject Instantiate();
    }
}