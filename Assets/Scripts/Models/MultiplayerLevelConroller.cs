﻿using System;
using Assets.Scripts;
using Assets.Scripts.network;
using BubblesGame.Models.Player;
using UnityEngine;

namespace BubblesGame.Models
{
    /// <summary>
    /// Базовый мультиплеерный контроллер. Отличается от обычного тем, что хранит в себе GameBroadcaster,
    ///  а так же дает доступ к завершению извне EndGameExternally()
    /// </summary>
    class MultiplayerLevelConroller : SinglePlayerLevelController
    {
        protected GameBroadcaster GameBroadcaster;
        
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="player">модель игрока</param>
        /// <param name="broadcaster">броадкастер сетевой игры</param>
        public MultiplayerLevelConroller(PlayerModel player, GameBroadcaster broadcaster) : base(player)
        {
            GameBroadcaster = broadcaster;
            Facade.I.Net.OnConnectionRefuse += OnConnectionRefuse;
        }

        /// <summary>
        /// Завершить игру. Вызывается в тот момент, когда другой игрок проиграл
        /// </summary>
        public void EndGameExternally()
        {
            End();
        }

        private void OnConnectionRefuse()
        {
            EntryPoint.MakeOnNextUpdate(() =>
            {
                Debug.Log("End game by connection fail");
                End();
            });
        }

        protected override void End()
        {
            Facade.I.Net.OnConnectionRefuse -= OnConnectionRefuse;
            base.End();
        }
    }
}