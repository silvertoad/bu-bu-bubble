﻿using System;
using System.Collections;
using System.Collections.Generic;
using BubblesGame.GUI;
using BubblesGame.GUI.Screens;

namespace Assets.Scripts.Models
{
    /// <summary>
    /// Менеджер игровых экранов
    /// </summary>
    class ScreenManager
    {
        private readonly ScreenFactory _factory;
        private IScreen _currentScreen;

        public ScreenManager()
        {
            _factory = new ScreenFactory();
        }

        /// <summary>
        /// Инициализатор
        /// </summary>
        /// <param name="firstScreen">тип первого отображаемого экрана</param>
        /// <param name="param">параметры первого экрана</param>
        public void Init(ScreenType firstScreen, Hashtable param)
        {
            Show(firstScreen, param);
        }

        /// <summary>
        /// Отобразить игровой экран
        /// </summary>
        /// <param name="type">тип экрана</param>
        /// <param name="param">параметры экрана</param>
        private void Show(ScreenType type, Hashtable param)
        {
            _currentScreen = _factory.CreateScreen(type);
            _currentScreen.Exit += OnCurrentScreenExit;
            _currentScreen.Init(param);
        }

        private void OnCurrentScreenExit(ScreenExitEvent obj)
        {
            var newScreen = obj.Type;
            Show(newScreen, obj.Param);
        }

    }

    /// <summary>
    /// Фабрика игровых экранов
    /// </summary>
    class ScreenFactory
    {
        private readonly Dictionary<ScreenType, Type> _container = new Dictionary<ScreenType, Type>();
        
        public ScreenFactory() 
        {
            _container.Add(ScreenType.Login, typeof(LoginScreen));
            _container.Add(ScreenType.MainMenu, typeof(MainMenuScreen));
            _container.Add(ScreenType.SinglePlayer, typeof(SinglePlayerScreen));
            _container.Add(ScreenType.NetworkMenu, typeof(NetworkMenuScreen));
            _container.Add(ScreenType.MultiPlayer, typeof(MultiPlayerScreen));
            _container.Add(ScreenType.FinishSinglePlayer, typeof(FinishSingleplayerGameScreen));
            _container.Add(ScreenType.FinishMultiPlayer, typeof(FinishMultiplayerGameScreen));
            _container.Add(ScreenType.ExitGame, typeof(ExitDialogScreen));
            _container.Add(ScreenType.Inbox, typeof(InboxScreen));
            _container.Add(ScreenType.OutboxConnection, typeof(OutboxConnectionScreen));
        }

        /// <summary>
        /// Создать игровой экран по типу
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public IScreen CreateScreen(ScreenType type)
        {
            Type typeValue;
            if (_container.TryGetValue(type, out typeValue))
                return (IScreen) Activator.CreateInstance(typeValue);
            throw new Exception("unknown screen type " + type);
        }

    }
}