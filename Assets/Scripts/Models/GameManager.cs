﻿using System;
using BubblesGame.Models.Game.LevelGeneration;
using BubblesGame.Utils;
using BubblesGame.View;
using UnityEngine;

namespace BubblesGame.Models
{
    /// <summary>
    /// Менеджер игрового процесса
    /// </summary>
    class GameManager
    {
        /// <summary>
        /// Игра закончиласть
        /// </summary>
        public event Action GameOver = delegate { };

        /// <summary>
        /// Старт нового уровня
        /// </summary>
        public event Action<GameLevel> LevelStarted = delegate { }; 

        private readonly GameObject _gameGo;
        private readonly LevelView _levelView;
        private readonly SinglePlayerLevelController _controller;
        private readonly ResourceManager _resourceManager;
        private readonly LevelGenerator _levelGenerator;
        private readonly BallViewFactory _ballViewFactory;

        private int _level = 1;
        private int _levelSeed;
        private GameLevel _gameLevel;
        
        /// <summary>
        /// Отображение уровня
        /// </summary>
        public LevelView LevelView { get { return _levelView; } }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="controller">игровой контроллер</param>
        public GameManager(SinglePlayerLevelController controller)
        {
            _gameGo = new GameObject();
            _resourceManager = new ResourceManager(BundleWrapper.Bundle);

            _levelGenerator = new LevelGenerator();

            _levelView = _gameGo.AddComponent<LevelView>();
            _levelView.gameObject.AddComponent<AudioSource>();
            _levelView.SetResourceManager(_resourceManager);

            _ballViewFactory = new BallViewFactory(_resourceManager);

            _controller = controller;
            _controller.Complete += OnLevelComplete;
            _controller.GameOver += OnGameOver;
        }

        /// <summary>
        /// Cтарт игры
        /// </summary>
        /// <param name="levelSeed"></param>
        public void Start(int levelSeed)
        {
            _levelSeed = levelSeed;
            _gameLevel = _levelGenerator.CreateLevel(_levelSeed, _level);

            _resourceManager.CreateSet(_gameLevel.ViewDataSet);
            _resourceManager.SetCreated += OnFirstSetCreated;
        }

        /// <summary>
        /// "Деструктор"
        /// </summary>
        public void Destroy()
        {
            GameObject.DestroyObject(_gameGo);
            _controller.Complete -= OnLevelComplete;
            _controller.GameOver -= OnGameOver;
        }

        private void OnLevelEnded()
        {
            ClearGame();
        }

        private void OnFirstSetCreated()
        {
            Debug.Log("FIRST SET CREATED");
            _resourceManager.SetCreated -= OnFirstSetCreated;
            _resourceManager.SwitchSet();
            StartLevel(_gameLevel);
            PrepareNextLevel();
        }

        private void PrepareNextLevel()
        {
            _level++;
            _levelSeed++;
            _gameLevel = _levelGenerator.CreateLevel(_levelSeed, _level);
            _resourceManager.CreateSet(_gameLevel.ViewDataSet);
            _resourceManager.SetCreated += OnSetCreated;
        }

        private void OnSetCreated()
        {
            Debug.Log("SET CREATED (LVL = " + _level + ")");
            _resourceManager.SetCreated -= OnSetCreated;
        }

        private void StartLevel(GameLevel level)
        {
            _controller.SetLevel(level);
            LevelView.Init(level, _controller, _ballViewFactory);
            LevelStarted(level);
        }

        private void ClearGame()
        {
            _levelView.Clear();
        }

        private void OnLevelComplete()
        {
            Debug.Log("level complete, start level " + _level);
            ClearGame();
            _resourceManager.SwitchSet();
            StartLevel(_gameLevel);

            PrepareNextLevel();
        }

        private void OnGameOver()
        {
            Debug.Log("GAME OVER");
            ClearGame();
            GameOver();
        }
    }
}
