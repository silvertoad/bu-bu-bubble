using System;
using System.Collections.Generic;
using BubblesGame.Models.Game.LevelGeneration;

namespace Assets.Scripts.Models.Game.LevelGeneration.Templates
{
    /// <summary>
    /// 
    ///       *     *   
    ///
    ///  *                *
    ///    *           *
    ///        *   *  
    ///  
    /// </summary>
    internal class TemplateSmile : ITemplate
    {
        public void ApplyTo(List<GameAction> actions, TimeProvider timeProvider, GeneratorUnit unit)
        {
            timeProvider.AddTime(2f);

            var minAngle = 0.5f;
            var maxAngle = (float) Math.PI - 0.5f;
            var count = 5;
            var delta = (maxAngle - minAngle)/count;
            var R = 0.2f;
            for (float angle = minAngle; angle <= maxAngle; angle += delta)
            {
                actions.Add(addByAngle(angle, R, unit));
            }

            var d = 0.4f;
            var topAngle = (float) (3*Math.PI/2);

            actions.Add(addByAngle(topAngle + d, R, unit));
            actions.Add(addByAngle(topAngle - d, R, unit));

            timeProvider.AddTime(2f);
        }

        private GameAction addByAngle(float angle, float r, GeneratorUnit unit)
        {
            var x = 0.5f + (float) Math.Cos(angle)*r;
            var y = (float) Math.Sin(angle)*r - r;
            return unit.GetOneAt(x, y, 0.1f);
        }
    }
}