using System.Collections.Generic;
using BubblesGame.Models.Game.LevelGeneration;

namespace Assets.Scripts.Models.Game.LevelGeneration.Templates
{
    /// <summary>
    /// ���� � ���
    /// </summary>
    internal class TemplateRow : ITemplate
    {
        private readonly int _count;
        private readonly float _x;
        private readonly float _deltaX;

        /// <summary>
        /// �����������
        /// </summary>
        /// <param name="count"> ���������� �����</param>
        /// <param name="x">���������� ������ ����</param>
        /// <param name="deltaX">���������� ����� ������</param>
        public TemplateRow(int count, float x, float deltaX)
        {
            _count = count;
            _x = x;
            _deltaX = deltaX;
        }

        public void ApplyTo(List<GameAction> actions, TimeProvider timeProvider, GeneratorUnit unit)
        {
            unit.GetLine(_count, _x - _deltaX*_count/2, _deltaX);
        }
    }
}