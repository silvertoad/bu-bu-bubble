﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BubblesGame.Models.Game.LevelGeneration;
using BubblesGame.Utils;

namespace Assets.Scripts.Models.Game.LevelGeneration.Templates
{
    /// <summary>
    /// Темплейт для пресетов шары в которых можно привязать к сетке
    /// </summary>
    class TetrisLikeTemplate : ITemplate
    {
        private readonly byte[][] _source;
        private readonly float _deltaX;
        private readonly float _size;
        private readonly Random _random;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="source">двумерный массив сетки шаров, где любое число > 0 -- шар</param>
        /// <param name="deltaX">расстояние между шарами</param>
        /// <param name="size">размер шара</param>
        /// <param name="random">генератор случайных чисел</param>
        public TetrisLikeTemplate(byte[][] source, float deltaX, float size, Random random)
        {
            _random = random;
            _size = size;
            _deltaX = deltaX;
            _source = source;
        }

        public void ApplyTo(List<GameAction> actions, TimeProvider timeProvider, GeneratorUnit unit)
        {
            float deltaTime = (_deltaX)/unit.CalculateSpeed(_size);
            float xShift = -_deltaX*(_source[0].Length - 1)/2;
            float yShift = -_deltaX*_source.Length;
            float width = _deltaX*_source[0].Length + _size;
            float x = _random.Range(width, 1-width);


            for (int i = 0; i < _source.Length; i++)
            {
                for (int j = 0; j < _source[i].Length; j++)
                {
                    if (_source[i][j] > 0)
                    {
                        actions.Add(unit.GetOneAt(xShift + x + j*_deltaX, yShift, _size));   
                    }
                }
                timeProvider.AddTime(deltaTime);
            }
        }
    }
}
