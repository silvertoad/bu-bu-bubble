using System.Collections.Generic;
using BubblesGame.Models.Game.LevelGeneration;

namespace Assets.Scripts.Models.Game.LevelGeneration.Templates
{
    /// <summary>
    /// ��������� ��� �.�. "���������" -- ������� �� ������������ ������� ������������ ����� ������������ �� ����� ����
    /// </summary>
    internal interface ITemplate
    {
        /// <summary>
        /// ��������� ������
        /// </summary>
        /// <param name="actions">������ ������� � ������� ����� ��������� ������ �������</param>
        /// <param name="timeProvider"></param>
        /// <param name="unit"></param>
        void ApplyTo(List<GameAction> actions, TimeProvider timeProvider, GeneratorUnit unit);
    }
}