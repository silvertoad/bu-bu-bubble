using System.Collections.Generic;
using BubblesGame.Models.Game.LevelGeneration;

namespace Assets.Scripts.Models.Game.LevelGeneration.Templates
{
    /// <summary>
    ///  * * *
    ///   * *
    ///    *
    /// </summary>
    internal class TemplatePyramid : ITemplate
    {
        private readonly float _deltaTime;
        private readonly int _rowsCount;

        /// <summary>
        /// �����������
        /// </summary>
        /// <param name="deltaTime">����� ����� ���������� ����� ��������</param>
        /// <param name="rowsCount">���������� ����� ��������</param>
        public TemplatePyramid(float deltaTime, int rowsCount)
        {
            _deltaTime = deltaTime;
            _rowsCount = rowsCount;
        }

        public void ApplyTo(List<GameAction> actions, TimeProvider timeProvider, GeneratorUnit unit)
        {
            var size = (unit.MinSize + unit.MaxSize)/2;
            for (int i = 0; i <= _rowsCount; i++)
            {
                actions.AddRange(unit.GetLine(i, 0.5f - (i + 0.5f)*size/2, size));
                timeProvider.AddTime(_deltaTime);
            }
        }
    }
}