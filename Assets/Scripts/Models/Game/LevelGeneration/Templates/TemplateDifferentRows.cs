using System.Collections.Generic;
using BubblesGame.Models.Game.LevelGeneration;

namespace Assets.Scripts.Models.Game.LevelGeneration.Templates
{
    /// <summary>
    ///   *   
    ///   *      *
    ///   *   
    ///   *      *
    ///   *   
    /// </summary>
    internal class TemplateDifferentRows : ITemplate
    {
        private readonly float _deltaTime;
        private readonly int _count;

        /// <summary>
        /// �����������
        /// </summary>
        /// <param name="deltaTime"> ����� ����� ���������� �����</param>
        /// <param name="count"> ���������� �����</param>
        public TemplateDifferentRows(float deltaTime, int count)
        {
            _deltaTime = deltaTime;
            _count = count;
        }

        public void ApplyTo(List<GameAction> actions, TimeProvider timeProvider, GeneratorUnit unit)
        {
            var everyN = _count/3;
            var firstAdd = _count/3;
            var size = unit.RandomSize();
            for (int i = 0; i < _count; i++)
            {
                if (i >= firstAdd && (everyN == 0 || ((i - firstAdd)%everyN) == 0))
                {
                    if (everyN > 0)
                        everyN--;
                    actions.Add(unit.GetOne(size, 0.8f));
                }
                actions.Add(unit.GetOne(size, 0.2f));
                timeProvider.AddTime(_deltaTime);
            }
        }
    }
}