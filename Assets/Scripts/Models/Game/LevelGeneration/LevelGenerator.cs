﻿using System;
using System.Linq;
using Assets.Scripts.Models.Game.LevelGeneration.Templates;
using BubblesGame;
using System.Collections.Generic;
using BubblesGame.Utils;
using Random = System.Random;
using UnityEngine;

namespace BubblesGame.Models.Game.LevelGeneration
{
    /// <summary>
    /// Генератор уровней игры
    /// </summary>
    internal class LevelGenerator
    {
        private TimeProvider _timeProvider;
        private Random _random;
        private GeneratorUnit _unit;
        private List<GameAction> _gameActions;
        private int _level;

        /// <summary>
        /// Создать модель игрового уровня
        /// </summary>
        /// <param name="seed">зерно уровня</param>
        /// <param name="levelNum">порядковый номер уровня</param>
        /// <returns></returns>
        public GameLevel CreateLevel(int seed, int levelNum)
        {
            _level = levelNum;
            _timeProvider = new TimeProvider();
            _random = new Random(seed);
            _unit = new GeneratorUnit(new UniqIdGenerator(), _timeProvider, _random, new DepthProvider());
            _gameActions = new List<GameAction>();

            //параметр сложности уровня от 0 до 1
            //растет линейно до 10 уровня, потом выходит на полку
            var complexity = Math.Min(1, (float) levelNum/10);

            //выставляем зависимости параметров от сложности
            _unit.MaxSpeed = 0.3f + complexity*0.2f;
            _unit.MinSpeed = 0.1f + complexity*0.3f;

            _unit.MaxSize = 0.2f - complexity*0.1f;
            _unit.MinSize = 0.1f - complexity*0.05f;

            _unit.MaxTime = 1f - complexity*0.5f;
            _unit.MinTime = 0.5f - complexity*0.2f;

            //регистрируем пресеты, выставляем им ограничения по уровням
            var presetChooser = new RandomChooser<ITemplate>();
            presetChooser.AddChoice(new TemplatePyramid(2, 3), EnableOnlyAtLevel(1));
            presetChooser.AddChoice(new TemplatePyramid(2, 4), EnableOnlyAtLevel(2));
            presetChooser.AddChoice(new TemplatePyramid(2, 5), EnableFromLevel(3));

            presetChooser.AddChoice(new TemplateRow(2, 0.5f, 0.1f), EnableFromLevel(2));
            presetChooser.AddChoice(new TemplateRow(3, 0.5f, 0.1f), EnableFromLevel(3));
            presetChooser.AddChoice(new TemplateRow(4, 0.5f, 0.1f), EnableFromLevel(4));
            presetChooser.AddChoice(new TemplateRow(5, 0.5f, 0.1f), EnableFromLevel(4));
            presetChooser.AddChoice(new TemplateRow(5, 0.5f, 0.1f), EnableFromLevel(4));

            presetChooser.AddChoice(new TemplateSmile(), EnableFromLevel(3));

            presetChooser.AddChoice(new TemplateDifferentRows(0.5f, 18), EnableFromLevel(5));

            var mainChooser = new RandomChooser<RandomChooser<ITemplate>>();
            mainChooser.AddChoice(presetChooser, 1);
            mainChooser.AddChoice(getTetrisTemplatesChooser(), 1);

            
            //в начале 3 уровня всегда появляется смайлик
            if (levelNum == 3)
                AddTemplate(new TemplateSmile());

            // алгоритм такой: 5 секунд рандома чередуется с рандомным пресетом (для текущего уровня)
            var i = 0;
            while (_timeProvider.GetTime() < 20)
            {
                if (i%2 == 0)
                {
                    AddRange(_unit.GetRandomFlow(5));
                }
                else
                {
                    var chooser = mainChooser.MakeChoice((float) _random.NextDouble());
                    var choice = chooser.MakeChoice((float) _random.NextDouble());
                    AddTemplate(choice);
                }
                //время между пресетом и рандомным потоком шаров уменьшается с ростом уровня
                _timeProvider.AddTime(1f - 0.9f*complexity);
                i++;
            }

            return new GameLevel(_gameActions);
        }

        /// <summary>
        /// Добавить список экшенов
        /// </summary>
        /// <param name="range"></param>
        private void AddRange(IEnumerable<GameAction> range)
        {
            _gameActions.AddRange(range);
        }

        /// <summary>
        /// Добавить темлпейт
        /// </summary>
        /// <param name="template"></param>
        private void AddTemplate(ITemplate template)
        {
            template.ApplyTo(_gameActions, _timeProvider, _unit);
        }

        /// <summary>
        /// Метод возвращающий 1 если текущий уровень больше либо равен необходимому, иначе 0
        /// Удобно пользоваться для задавания веса пресета (если хотим чтобы пресет был ограничен необходимым уровнем)
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        private int EnableFromLevel(int level)
        {
            return level <= _level ? 1 : 0;
        }

        /// <summary>
        /// Метод возвращающий 1 если текущий уровень равен необходимому, иначе 0
        /// Удобно пользоваться для задавания веса пресета (если хотим чтобы пресет был ограничен необходимым уровнем)
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        private int EnableOnlyAtLevel(int level)
        {
            return level == _level ? 1 : 0;
        }

        private RandomChooser<ITemplate> getTetrisTemplatesChooser()
        {
            var chooser = new RandomChooser<ITemplate>();
            var figures = new []
            {
                new[]
                {
                    new byte[] {1, 0, 0},
                    new byte[] {1, 1, 1}
                },
                new[]
                {
                    new byte[] {1, 1},
                    new byte[] {1, 1}
                },
                new[]
                {
                    new byte[] {1},
                    new byte[] {1},
                    new byte[] {1},
                    new byte[] {1}
                },
                new[]
                {
                    new byte[] {1, 0},
                    new byte[] {1, 1},
                    new byte[] {0, 1}
                },
                new[]
                {
                    new byte[] {0, 1, 1},
                    new byte[] {1, 1, 0}
                },
                new[]
                {
                    new byte[] {1, 0},
                    new byte[] {1, 0},
                    new byte[] {1, 1}
                } 
            };

            foreach (byte[][] figure in figures)
            {

                chooser.AddChoice(new TetrisLikeTemplate(figure, _unit.MaxSize, _unit.MaxSize, _random), 1);
            }
            return chooser;
        }
    }
}