namespace BubblesGame.Models.Game.LevelGeneration
{
    /// <summary>
    /// ����� ��������������� ������ � ���������������������� �������� ������� (z-����������) ��� ��������� ����� �����
    /// </summary>
    internal class DepthProvider
    {
        private float _depth = 0f;
        private readonly float _maxDepth;

        /// <summary>
        /// �����������
        /// </summary>
        /// <param name="maxDepth"> ������������ �������� �������, ��� ���������� �������� ��������� ��������� �� ����</param>
        public DepthProvider(float maxDepth = 3)
        {
            _maxDepth = maxDepth;
        }

        /// <summary>
        /// ����� ��������� �������
        /// </summary>
        /// <param name="toAdd">����� �������� ����� �������� � �����������</param>
        /// <returns>����� ������� �������</returns>
        public float GetDepth(float toAdd)
        {
            _depth += toAdd;
            if (_depth > _maxDepth)
                _depth = 0;
            return _depth;
        }
    }
}