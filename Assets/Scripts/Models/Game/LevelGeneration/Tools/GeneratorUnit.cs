using System;
using System.Collections.Generic;
using BubblesGame.Utils;

namespace BubblesGame.Models.Game.LevelGeneration
{
    /// <summary>
    /// �����-�������� �������� ����� ����� ��� ������
    /// </summary>
    internal class GeneratorUnit
    {
        private readonly UniqIdGenerator _idMaker;
        private readonly TimeProvider _time;
        private readonly Random _random;
        private readonly DepthProvider _depth;

        /// <summary>
        /// ������������ �������� ���������� X ���������� ����
        /// </summary>
        public float MaxX = 1f;

        /// <summary>
        /// ����������� �������� ���������� X ���������� ����
        /// </summary>
        public float MinX = 0f;

        /// <summary>
        /// ������������ ������ ���������� ����
        /// </summary>
        public float MaxSize = 0.2f;

        /// <summary>
        /// ����������� ������ ���������� ����
        /// </summary>
        public float MinSize = 0.1f;

        /// <summary>
        /// ����������� �������� ���������� ����
        /// </summary>
        public float MinSpeed = 0.1f;

        /// <summary>
        /// ������������ �������� ���������� ����
        /// </summary>
        public float MaxSpeed = 0.2f;

        /// <summary>
        /// ����������� ����� ��������� ����� ����� ���������� ������
        /// </summary>
        public float MinTime = 0.5f;

        /// <summary>
        /// ������������ ����� ��������� ����� ����� ���������� ������
        /// </summary>
        public float MaxTime = 1f;

        /// <summary>
        /// �����������
        /// </summary>
        /// <param name="idMaker"></param>
        /// <param name="time"></param>
        /// <param name="random"></param>
        /// <param name="depth"></param>
        public GeneratorUnit(UniqIdGenerator idMaker, TimeProvider time, Random random, DepthProvider depth)
        {
            _idMaker = idMaker;
            _time = time;
            _random = random;
            _depth = depth;
        }

        /// <summary>
        /// ������ �������������� ������� ���������� ����
        /// </summary>
        public float ConstSize
        {
            set { MinSize = MaxSize = value; }
        }

        /// <summary>
        /// ������ �������������� ������� ��������� ����� ����� ���������� ������
        /// </summary>
        public float ConstTime
        {
            set { MinTime = MaxTime = value; }
        }

        /// <summary>
        /// ������������� ��������� ������ ����
        /// </summary>
        /// <returns></returns>
        public float RandomSize()
        {
            return _random.Range(MinSize, MaxSize);
        }

        /// <summary>
        /// ������������� ��������� ���������� � ����
        /// </summary>
        /// <param name="size">������ ����</param>
        /// <returns></returns>
        public float RandomX(float size)
        {
            return _random.Range(Math.Max(MinX, size), Math.Min(MaxX, 1 - size));
        }

        /// <summary>
        /// ������������� ���� ��������� ���������� ����
        /// </summary>
        /// <returns></returns>
        public GameAction GetOne()
        {
            var size = RandomSize();
            return GetOne(size);
        }

        /// <summary>
        /// ������������� ���� ��������� ���������� ����
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public GameAction GetOne(float size)
        {
            return GetOne(size, RandomX(size));
        }

        /// <summary>
        /// ������������� ���� ��������� ���������� ����
        /// </summary>
        /// <param name="size"></param>
        /// <param name="x"></param>
        /// <returns></returns>
        public GameAction GetOne(float size, float x)
        {
            return GetOneAt(x, -size, size);
        }

        /// <summary>
        /// ������������� ���� ��������� ���������� ����
        /// </summary>
        /// <returns></returns>
        public GameAction GetOneAt(float x, float y, float size)
        {
            var speed = CalculateSpeed(size);
            var z = _depth.GetDepth(size);
            var param = new BallModel(x, y, z, speed, size, _idMaker.GetUniqId());
            var action = new GameAction(_time.GetTime(), GameActionType.AddObject, param);
            return action;
        }

        /// <summary>
        /// ������������� ������ ������� �������� ��������� ����� � �������� �������
        /// </summary>
        /// <param name="flowTime">�����, � ������� �������� ����� ���������� ����</param>
        /// <returns></returns>
        public IEnumerable<GameAction> GetRandomFlow(float flowTime)
        {
            var retval = new List<GameAction>();
            var startTime = _time.GetTime();
            var endTime = startTime + flowTime;

            while (_time.GetTime() < endTime)
            {
                var deltaTime = _random.Range(MinTime, MaxTime);
                var size = _random.Range(MinSize, MaxSize);
                var speed = CalculateSpeed(size);
                var x = _random.Range(size, 1f - size);
                var z = _depth.GetDepth(size);
                var y = -size;
                var param = new BallModel(x, y, z, speed, size, _idMaker.GetUniqId());

                var actionTime = _time.AddTime(deltaTime);

                var action = new GameAction(actionTime, GameActionType.AddObject, param);

                retval.Add(action);
            }
            return retval;
        }

        /// <summary>
        /// �������� ������ ������� ������������ ������������ ����� ����������� � �������������� �����
        /// </summary>
        /// <param name="count">���������� �����</param>
        /// <param name="x">���������� ������ ����</param>
        /// <param name="deltaX">����������� ����� ������ �� �����������</param>
        /// <returns></returns>
        public IEnumerable<GameAction> GetLine(int count, float x = 0f, float deltaX = 0.1f)
        {
            var retval = new List<GameAction>();
            for (int i = 0; i < count; i++)
            {
                retval.Add(GetOne((MinSize + MaxSize)/2, x + i*deltaX));
            }
            return retval;
        }

        /// <summary>
        /// ���������� �������� ��� ����
        /// </summary>
        /// <param name="size">������ ����</param>
        /// <returns></returns>
        public float CalculateSpeed(float size)
        {
            if (Math.Abs(MaxSize - MinSize) < 0.0001f)
                return MinSpeed + (MaxSpeed - MinSpeed)/2;
            return MinSpeed + (MaxSpeed - MinSpeed)*(MaxSize - size)/(MaxSize - MinSize);
        }
    }
}