namespace BubblesGame.Models.Game.LevelGeneration
{
    /// <summary>
    /// ����� ��������������� ������ � ���������������� �������� ������� � ������ ��������� ������
    /// </summary>
    internal class TimeProvider
    {
        private float _time = 0f;

        public TimeProvider()
        {
        }

        /// <summary>
        /// ��������� �������� �������� �������
        /// </summary>
        /// <returns></returns>
        public float GetTime()
        {
            return _time;
        }

        /// <summary>
        /// ����� �������� ������� ������
        /// </summary>
        /// <param name="deltaTime">�������� ������</param>
        /// <returns>����� �������� �������</returns>
        public float AddTime(float deltaTime)
        {
            _time += deltaTime;
            return _time;
        }
    }
}