using System;
using System.Collections.Generic;

namespace BubblesGame.Models.Game.LevelGeneration
{
    /// <summary>
    /// ����� ����������� ������� ��������� ����� �� ������
    /// </summary>
    /// <typeparam name="T">��� ���������� ��������</typeparam>
    internal class RandomChooser<T>
    {
        private readonly List<T> _choices = new List<T>();

        /// <summary>
        /// �������� ������ ������ � ������
        /// </summary>
        /// <param name="obj">������ ������</param>
        /// <param name="weight">��� �����������, ��� ������, ��� ��������� ����� ������ ����� �������</param>
        public void AddChoice(T obj, int weight)
        {
            for (int i = 0; i < weight; i++)
            {
                _choices.Add(obj);
            }
        }

        /// <summary>
        /// ������� �����
        /// </summary>
        /// <param name="random">����� �� 0 �� 1</param>
        /// <returns>��������� ������</returns>
        public T MakeChoice(float random)
        {
            return _choices[(int) Math.Floor(_choices.Count*random)];
        }
    }
}