namespace BubblesGame.Models.Game.LevelGeneration
{
    /// <summary>
    /// ��������� ����������� id (������������ ������������� � ������ ������ ����������
    /// </summary>
    internal class UniqIdGenerator
    {
        private int _lastId = 0;

        public UniqIdGenerator()
        {
        }

        /// <summary>
        /// �������� id
        /// </summary>
        /// <returns></returns>
        public int GetUniqId()
        {
            _lastId++;
            return _lastId;
        }
    }
}