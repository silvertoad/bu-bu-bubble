﻿using System;
using BubblesGame.Models.Interfaces;
using UnityEngine;
using Random = UnityEngine.Random;

namespace BubblesGame.Models.Game.LevelGeneration
{
    /// <summary>
    /// Модель игрового шара
    /// </summary>
    public class BallModel : IRewindable, IGameObjectModel
    {
        public event Action Removed = delegate { };
        public event Action Moved = delegate { };

        public const float MinXpos = -10;
        public const float MaxXpos = 10;
        public const float MinYpos = 0;
        public const float MaxYpos = 10;
        public const float MaxSize = 0.2f;
        public const float MinSize = 0.1f;

        public readonly float Speed;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <param name="speed"></param>
        /// <param name="size"></param>
        /// <param name="id"></param>
        public BallModel(float x, float y, float z, float speed, float size, int id)
        {
            X = x;
            Y = y;
            Z = z;
            Size = size;
            Clr = new Color(Random.value, Random.value, Random.value);
            Id = id;

            //чем больше скорость, тем больше очков, причем зависимость квадратичная
            //дополнительно делаем чтобы на конце числа была цифра 0 (для красоты)
            Score = 10*((int) Math.Pow(speed*100, 2)/100);

            this.Speed = speed;
        }

        public new string ToString()
        {
            return "BallModel " + X + " " + Y + " " + Z + " " + Size;
        }

        public float X { get; private set; }

        public float Y { get; private set; }

        public float Z { get; private set; }

        public int Id { get; private set; }

        public float Size { get; private set; }

        public int Score { get; private set; }

        public Color Clr { get; private set; }

        public void Remove()
        {
            Removed();
        }

        /// <summary>
        /// Применить на шар "сдвиг времени"
        /// </summary>
        /// <param name="deltaTime"></param>
        public void Rewind(float deltaTime)
        {
            SetCoods(X, Y + deltaTime*Speed, Z);
        }

        public Int16 TextureSize
        {
            get
            {
                if (Size < MinSize + (MaxSize - MinSize) / 4.0)
                {
                    return ResourceManager.TEXTURE_64;
                }
                else if (Size < MinSize + (MaxSize - MinSize) / 2.0)
                {
                    return ResourceManager.TEXTURE_128;
                }
                return ResourceManager.TEXTURE_256;
            }
        }

        private void SetCoods(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
            Moved();
        }
    }
}