﻿using System;

namespace BubblesGame.Models.Game.LevelGeneration
{
    internal enum GameActionType
    {
        AddObject
    }

    /// <summary>
    /// VO игрового экшена
    /// Игровые экшены определяют игровой уровень. Действия игрока не являются экшенами, так как экшены всегда определены заранее
    /// </summary>
    internal class GameAction
    {
        /// <summary>
        /// Время в секундах
        /// </summary>
        public readonly float time;

        /// <summary>
        /// Тип экшена
        /// </summary>
        public readonly GameActionType type;

        /// <summary>
        /// Параметры экшена, зависят от типа
        /// </summary>
        public readonly Object param;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="time"></param>
        /// <param name="type"></param>
        /// <param name="param"></param>
        public GameAction(float time, GameActionType type, Object param)
        {
            this.time = time;
            this.type = type;
            this.param = param;
        }
    }
}