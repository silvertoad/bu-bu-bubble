﻿using System;
using System.Collections;
using System.Collections.Generic;
using BubblesGame.Models.Interfaces;
using UnityEngine;

namespace BubblesGame.Models.Game.LevelGeneration
{
    /// <summary>
    /// Класс игрового уровня
    /// </summary>
    internal class GameLevel : IRewindable, IGameLevel
    {
        public event Action<IGameObjectModel> NewObjectCreated = delegate { };
        public event Action<IGameObjectModel> ObjectFeltDown = delegate { };
        public event Action<IGameObjectModel> ObjectRemoved = delegate { };
        public event Action LevelEnded = delegate { };

        private readonly Dictionary<int, BallModel> _objectStorage;

        public readonly List<GameAction> Actions;

        private int _position = 0;
        private float _time = 0;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="actions">Список игровых экшенов</param>
        public GameLevel(List<GameAction> actions)
        {
            Ended = false;
            this.Actions = actions;
            _objectStorage = new Dictionary<int, BallModel>();
        }

        /// <summary>
        /// Получение модели шара
        /// </summary>
        /// <param name="id">Идентификатор шара</param>
        /// <exception cref="Exception">Ball with id not found</exception>
        /// <returns></returns>
        public BallModel GetBallModel(int id)
        {
            var ball = _objectStorage[id];
            if (ball == null)
                throw new Exception("Ball with id " + id + " not found");
            return ball;
        }

        /// <summary>
        /// Завершен ли уровень
        /// </summary>
        public bool Ended { get; private set; }

        /// <summary>
        /// Текущее время уровня (в секундах от начала уровня)
        /// </summary>
        public float Time
        {
            get { return _time; }
        }

        /// <summary>
        /// Применить сдвиг времени. В момент сдвига времени уровень рассылает события произошедшие за это время 
        /// </summary>
        /// <param name="deltaTime">дельта времени в секундах</param>
        public void Rewind(float deltaTime)
        {
            _time = Time + deltaTime;

            var toRemove = new List<BallModel>();

            //применяем сдвиг времени к шарам
            foreach (var pair in _objectStorage)
            {
                var ballModel = pair.Value;
                ballModel.Rewind(deltaTime);
                if (NeedToRemove(ballModel))
                    toRemove.Add(ballModel);
            }

            //удаляем шары которые дошли до низа
            foreach (var ballModel in toRemove)
            {
                ObjectFeltDown(ballModel);
                DestroyBall(ballModel);
            }

            //если кончились экшены, проверяем на завершение
            if (_position >= Actions.Count)
            {
                CheckEnd();
                return;
            }

            //смотрим на список экшенов и выполняем те, которые подходят по времени
            for (var i = _position; i < Actions.Count; i++)
            {
                var action = Actions[i];
                if (action.time < Time)
                {
                    Act(action);
                    _position++;
                }
                else
                {
                    break;
                }
            }
        }

        /// <summary>
        /// Застрелить шар
        /// </summary>
        /// <param name="ballModel">модель шара</param>
        public void Shoot(BallModel ballModel)
        {
            DestroyBall(ballModel);
            ObjectRemoved(ballModel);
            CheckEnd();
        }

        /// <summary>
        /// Проверка на завершение уровня
        /// </summary>
        private void CheckEnd()
        {
            if (_position >= Actions.Count && _objectStorage.Count == 0)
            {
                End();
            }
        }

        /// <summary>
        /// удаление шара
        /// </summary>
        /// <param name="ballModel"></param>
        private void DestroyBall(BallModel ballModel)
        {
            ballModel.Remove();
            _objectStorage.Remove(ballModel.Id);
        }

        /// <summary>
        /// Подлежит ли шар удалению
        /// </summary>
        /// <param name="ball"></param>
        /// <returns></returns>
        private bool NeedToRemove(BallModel ball)
        {
            return ball.Y > 1 + ball.Size; //верхняя граница шара опустилась за нижнюю планку
        }

        /// <summary>
        /// завершение уровня
        /// </summary>
        private void End()
        {
            Ended = true;
            LevelEnded();
        }

        /// <summary>
        /// Выполнить экшен
        /// </summary>
        /// <param name="action"></param>
        private void Act(GameAction action)
        {
            BallModel ballModel;
            switch (action.type)
            {
                case GameActionType.AddObject: //появился новый шар
                    ballModel = (BallModel) action.param;
                    AddModel(ballModel);
                    ballModel.Rewind(Time - action.time);
                    NewObjectCreated(ballModel);
                    break;

                default:
                    throw new Exception("Unknown type " + action.type);
            }
        }

        private void AddModel(BallModel ballModel)
        {
            _objectStorage[ballModel.Id] = ballModel;
        }

        /// <summary>
        /// Получить словарь размеров и цветов шаров используемых в уровне
        /// </summary>
        public Dictionary<Int16, List<Color>> ViewDataSet
        {
            get
            {
                var res = new Dictionary<short, List<Color>>();

                foreach (var gameAction in Actions)
                {
                    var model = (BallModel) gameAction.param;
                    var size = model.TextureSize;

                    if (!res.ContainsKey(size))
                        res[size] = new List<Color>();

                    res[size].Add(model.Clr);
                }

                return res;
            }
        }
    }
}