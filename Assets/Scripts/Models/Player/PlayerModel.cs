﻿using System;
using BubblesGame.Models.Interfaces;
using Assets.Scripts.network.data;

namespace BubblesGame.Models.Player
{
    internal class PlayerModel : IPlayerModel
    {
        public event Action ScoreChanged = delegate { };
        public event Action HPChanged = delegate { };

        public int Score { get; private set; }
        public int HP { get; private set; }

        public IUserModel User { get; private set; }

        public PlayerModel(IUserModel user)
        {
            User = user;
            Score = 0;
            HP = 10;
        }

        /// <summary>
        /// Добавить hp
        /// </summary>
        /// <param name="value"></param>
        public void AddHP(int value)
        {
            HP += value;
            HPChanged();
        }

        /// <summary>
        /// Добавить очки
        /// </summary>
        /// <param name="value"></param>
        public void AddScore(int value)
        {
            Score += value;
            ScoreChanged();
        }

        /// <summary>
        /// Освобождение
        /// </summary>
        public void Dispose()
        {
            User = null;
        }
    }
}