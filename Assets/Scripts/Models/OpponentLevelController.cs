﻿using System.Collections.Generic;
using Assets.Scripts.network;
using Assets.Scripts.network.data;
using BubblesGame.Models.Player;

namespace BubblesGame.Models
{
    /// <summary>
    /// Контроллер оппонента основного игрока
    /// </summary>
    class OpponentLevelController : MultiplayerLevelConroller
    {

        private List<GameStepInfo> _recievedActionStack; 

        public OpponentLevelController(PlayerModel player, GameBroadcaster broadcaster) : base(player, broadcaster)
        {
            GameBroadcaster = broadcaster;
            _recievedActionStack = new List<GameStepInfo>();
            broadcaster.OnDataRecived += OnDataRecived;
            broadcaster.ReadSteam();
        }

        private void OnDataRecived(GameStepInfo gameStepInfo)
        {
            _recievedActionStack.Add(gameStepInfo);
        }

        public override void OnMouseDown(int id)
        {
            //нельзя кликать по полю оппонента
        }

        public override void Rewind(float deltaTime)
        {
            //разбираем экшены пришедшие от второго игрока
            for (int i = 0; i < _recievedActionStack.Count; i++)
            {
                var clickInfo = _recievedActionStack[i];
                if (clickInfo.ClickId > 0)
                {
                    base.OnMouseDown(clickInfo.ClickId);
                }
                else if (clickInfo.Time > 0)
                {
                    base.Rewind(clickInfo.Time);
                }
            }
            _recievedActionStack = new List<GameStepInfo>();
        }
    }
}
