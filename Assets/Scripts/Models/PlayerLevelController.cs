﻿using Assets.Scripts.network;
using Assets.Scripts.network.data;
using BubblesGame.Models.Player;

namespace BubblesGame.Models
{
    /// <summary>
    /// Контроллер основного игрока во время сетевой игры
    /// </summary>
    class PlayerLevelController : MultiplayerLevelConroller
    {
        /// <summary>
        /// минимальное время между отправкой пакетов другому игроку
        /// </summary>
        private const float MinSyncTime = 0f;

        /// <summary>
        /// время игры накопленное для отправки в следующем пакете
        /// </summary>
        private float _nonSyncedDeltaTime = 0f;

        public PlayerLevelController(PlayerModel player, GameBroadcaster broadcaster) : base(player, broadcaster)
        {
        }

        public override void OnMouseDown(int id)
        {
            base.OnMouseDown(id);  //просчитываем клик
            SendTimeSyncronize(); //отправляем несинхронизированное время
            var clickInfo = new GameStepInfo { ClickId = id };
            GameBroadcaster.SendGameStep(clickInfo); //отправляем клик другому игроку
        }

        protected override void End()
        {
            SendTimeSyncronize();
            base.End();
        }

        public override void Rewind(float deltaTime)
        {
            _nonSyncedDeltaTime += deltaTime;
            base.Rewind(deltaTime);
            if (_nonSyncedDeltaTime >= MinSyncTime)
            {
                SendTimeSyncronize();
            }
        }

        private void SendTimeSyncronize()
        {
            GameBroadcaster.SendGameStep(new GameStepInfo { Time = _nonSyncedDeltaTime });
            _nonSyncedDeltaTime = 0;
        }
    }
}
